﻿module test;

import std.stdio, std.conv, std.string, std.process, std.exception;

int main (string[] args) {
	ulong nb = to!ulong(args[1]);
	double total = 0, max=0, min=double.max;

	//string command = "/usr/bin/time -f \"%e\" ";
	string command = "TIMEFORMAT='%3U'; time";
	for (auto i=2; i<args.length; ++i) {
		command ~= (' ' ~ args[i]);
	}
	command ~= " > /dev/null";

	for (ulong i=0; i<nb; ++i) {
		try {
			auto ret = executeShell(command);
			if (ret.status != 0) {
				writeln("Return non-zero on try ", i+1);
				return 1;
			}
			//stderr.writeln(chomp(ret.output));
			double value = to!double(chomp(ret.output));
			total += value;
			if (value > max) max = value;
			if (value < min) min = value;
		} catch (Exception e) {
			writeln("Exception on try ", i+1);
			throw e;
			return 2;
		}
	}
	writeln(nb, " attempts:");
	writeln(" Mean: ", total/nb);
	writeln(" Max: ", max);
	writeln(" Min: ", min);
	writeln(" Total: ", total);

	return 0;
}