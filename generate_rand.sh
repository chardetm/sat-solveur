#!/bin/bash

if  [ $# -ne 4 ]
then
	echo "Mauvais nombre d'arguments : k, numvars, numclauses, numsamples"
else
	mkdir random_tests 2> /dev/null
	for k in $(seq 2 $1)
	do
		for numvars in $(seq $(($k>30?$k:30)) 5 $2)
		do
			for numclauses in $(seq 300 10 $3)
			do
				for numsample in $(seq 1 $4)
				do
					echo $k $numvars $numclauses N random_tests/rand-k$k-nv$numvars-nc$numclauses-$numsample.cnf
					#python random_ksat.py $k $numvars $numclauses N random_tests/rand-k$k-nv$numvars-nc$numclauses-$numsample.cnf
					./genksat $k $numvars $numclauses > random_tests/rand-k$k-nv$numvars-nc$numclauses-$numsample.cnf
				done
			done
		done
	done
fi