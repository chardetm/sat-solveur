﻿module genksat;

import std.stdio, std.conv, std.random;

void writeUsage (ref string prog_name) {
	writeln("Usage: ", prog_name, " k num_var num_cl");
	writeln(" - k: number of literals per clause");
	writeln(" - num_var: number of variables");
	writeln(" - num_cl: number of clauses");
}

int main (string[] args) {
	if (args.length != 4) {
		writeUsage(args[0]);
		return 1;
	}
	ulong k, num_var, num_cl;
	try {
		k = to!ulong(args[1]);
		num_var = to!ulong(args[2]);
		num_cl = to!ulong(args[3]);
	} catch (Exception e) {
		writeUsage(args[0]);
		return 1;
	}

	if (k>num_var) {
		writeln("Error: num_var must be greater than or equal to k!");
		return 2;
	}

	writeln("c A ", k, "-SAT satisfiable problem with ", num_var, " variables and ", num_cl, " clauses.");
	writeln("p cnf ", num_var, ' ', num_cl);

	bool[] solution;
	ulong[] probas;
	ulong[] probas2;
	solution.reserve(num_var);
	probas.reserve(num_var);
	immutable ulong proba = (3*((k*num_cl)/num_var+1))/2;
	foreach (i; 0..num_var) {
		solution~=((uniform!"[]"(0,1) == 1)?true:false);
		probas~=proba;
	}
	foreach (i; 0..num_cl) {
		long[] clause;
		bool satisf = false;
		probas2=probas.dup;

		foreach (j; 0..k) {
			long chosen = dice(probas2);
			--probas[chosen];
			probas2[chosen] = 0;
			bool positive = (uniform!"[]"(0,1) == 1)?true:false;
			if (positive == solution[chosen]) satisf = true;
			clause~=(positive ? chosen+1 : -(chosen+1));
		}
		if (!satisf) {
			long id=uniform!"[)"(0,k);
			clause[id] = -clause[id];
		}
		foreach (lit; clause) {
			write (lit, " ");
		}
		writeln("0");
	}
	return 0;
}