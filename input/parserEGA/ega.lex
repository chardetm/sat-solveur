%{

#include "global.h"
#include "ega.tab.h"

#include <stdlib.h>
#include <stdio.h>

%}
%option noyywrap

blanks      [ \t\r\n]+

digit       [0-9]
nzdigit     [1-9]
number      {digit}*{nzdigit}+{digit}*

%%

{number} {
	egalval=atoi(yytext);
	return NUMBER;
}
"ega" {return EGATOK;}

"(" { return LPAR;}
")" { return RPAR;}
"/\\" { return LAND;}
"\\/" {return LOR;}
"=>" {return LIMPL;}
"~" {return NEG;}
"=" {return EGA;}
"x" {return VAR;}
"!=" {return INEGA;}

{blanks} {}

. ;


%%
