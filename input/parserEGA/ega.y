%{

#include "global.h"
#include <stdio.h>
#include <stdlib.h>

extern int yyparse();
extern FILE* egain;

extern void addLitEGA(long long l1, long long l2, int ega);
extern void addAndEGA();
extern void addOrEGA();
extern void addImplEGA();
extern void addNegEGA();
%}

%token LPAR RPAR LAND LOR LIMPL NEG NUMBER EGATOK EGA VAR INEGA

%precedence LPAR RPAR
%right LIMPL
%left LAND
%left LOR
%precedence NEG

%start File
%%

File:
	EGATOK FormuleEga
;

FormuleEga:
	LPAR FormuleEga RPAR
	|FormuleEga LAND FormuleEga
	{
		addAndEGA();
	}
	|FormuleEga LOR FormuleEga
	{
		addOrEGA();
	}
	|FormuleEga LIMPL FormuleEga
	{
		addImplEGA();
	}
	|NEG FormuleEga
	{
		addNegEGA();
	}
	|VAR NUMBER EGA VAR NUMBER
	{
		addLitEGA($2, $5, 1);
	}
	|VAR NUMBER INEGA VAR NUMBER
	{
		addLitEGA($2, $5, 0);
	}
;

%%

int yyerror(char *s) {
}

int parseEGA(FILE* f) {
  egain = f;
  return yyparse();
}
