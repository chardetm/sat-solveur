module input.parsers.ega;

import types.clause, types.valuation, types.instance, types.basic_types, types.graphe, types.arbre;
import std.stdio, std.string;
import std.exception, std.container, std.conv, std.math;

private Arbre[] pile;
private var_t nbVariable;

private bool error;

extern(C)
{
	int parseEGA(FILE* f);

	void addLitEGA(long l1, long l2, int ega)
	{
		Arbre a = new Arbre("=");
		Arbre fils1 = new Arbre("x");
		Arbre fils2 = new Arbre("x");
		Arbre fils11 = new Arbre(to!string(l1));
		Arbre fils21 = new Arbre(to!string(l2));
		fils1.addFils(fils11);
		fils2.addFils(fils21);
		a.addFils(fils1);
		a.addFils(fils2);
		if(ega != 0)
			pile ~= a;
		else
		{
			Arbre t = new Arbre("~");
			t.addFils(a);
			pile ~= t;
		}
	}
	void addNegEGA()
	{
		Arbre fils = new Arbre("~");
		fils.addFils(pile[$-1]);
		pile[$-1] = fils;
	}
	void addAndEGA()
	{
		Arbre fils = new Arbre("/\\\\");
		fils.addFils(pile[$-2]);
		fils.addFils(pile[$-1]);
		pile.length--;
		pile[$-1] = fils;
	}
	void addOrEGA()
	{
		Arbre fils = new Arbre("\\\\/");
		fils.addFils(pile[$-2]);
		fils.addFils(pile[$-1]);
		pile.length--;
		pile[$-1] = fils;
	}
	void addImplEGA()
	{
		Arbre fils = new Arbre("\\\\/");
		Arbre fils1 = new Arbre("~");
		fils1.addFils(pile[$-2]);
		fils.addFils(fils1);
		fils.addFils(pile[$-1]);
		pile.length--;
		pile[$-1] = fils;
	}

}

Arbre parsingEga(ref File file)
{
	error = false;
	nbVariable = 0;
	pile.length = 0;
	parseEGA(file.getFP());

	if(error)
	{
		throw new ParsingException("Parsing error");
	}
	
	return pile[0];
}
