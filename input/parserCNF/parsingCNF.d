module input.parsers.cnf;

import types.clause, types.valuation, types.instance, types.basic_types, types.graphe, types.arbre;
import std.stdio, std.string;
import std.exception, std.container, std.conv, std.math;

private Instance inst;
private uint c, realc = 0;
private var_t v, realv = 0;

private bool error;
private bool clauseVide;

private Clause toAdd;

extern(C)
{
	int parseCNF(FILE* f);
	void reportError(char *s)
	{
		error = true;
	}
	void reportError2()
	{
		error = true;
	}

	void init(long vN, long cN)
	{
		c = cast(uint)cN;
		v = cast(var_t)vN;
	}
	void newClause()
	{
		toAdd = new Clause();
	}
	void addClause()
	{
		inst.addClause(toAdd);
		realc++;
		toAdd = new Clause();
	}
	void addLit(long lit)
	{
		var_t var = lit < 0 ? -lit : lit;
		if(var > realv)
			realv = var;
		toAdd.add_literal(lit);
	}
	void reportClauseVide()
	{
		clauseVide = true;
	}
}

Instance bisonParsing(ref File file, mode_t choix, pari_t pariChoix)
{
	error=false;
	clauseVide = false;
	c = 0;
	realc = 0;
	v = 0;
	realv = 0;
	inst = new Instance(choix, pariChoix);
	toAdd = new Clause();

	parseCNF(file.getFP());
	
	if (error) {
		throw new ParsingException("Parsing error");
	}
	else if(clauseVide)
	{
		throw new NotSolvable("Clause vide");
	}

	if (realc != c) {
		stderr.writeln("Warning: wrong number of clauses in the header (", realc, " clauses instead of ", c, ")");
		c =realc;
	}
	if (realv != v) {
		stderr.writeln("Warning: wrong number of variables in the header (", realv, " variables instead of ", v, ")");
		v = realv;
	}
	inst.init(v,c);
	Clause.init(inst.val);
	
	return inst;
}