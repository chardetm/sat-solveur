%{

#include "global.h"
#include "cnf.tab.h"

#include <stdlib.h>
#include <stdio.h>

%}
%option noyywrap

blanks      [ \t\r\n]+

digit       [0-9]
nzdigit     [1-9]
number      [\+-]?{digit}*{nzdigit}+{digit}*

subcomment  [\ {digit}\+-].*
comment     ^c{subcomment}?$

deberror	^[^pc0-9]

%%

"p" {return PTOK;}
"cnf" {return CNFTOK;}
"0" {return ZTOK;}

{number} {
	cnflval=atoi(yytext);
	return NUMBER;
}

{blanks} {}
{comment} {}
{deberror} {return ERROR;}

. ;


%%
