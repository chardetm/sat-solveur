%{

#include "global.h"
#include <stdio.h>
#include <stdlib.h>

extern int yyparse();
extern FILE* cnfin;

extern void reportError(char *s);
extern void reportError2();
extern void init(long long v, long long c);
extern void newClause();
extern void addLit(long long lit);
extern void addClause();
extern void reportClauseVide();


%}

%token PTOK CNFTOK NUMBER ZTOK ERROR

%start Clauses
%%

Clauses:
	Clauses Clause
	{
		addClause();
	}
	|Debut
	|ERROR
	{
		reportError2();
	}
;
Clause:
	Literals ZTOK
	|ZTOK
	{
		reportClauseVide();
	}
;
Literals:
	Literals NUMBER
	{
		addLit($2);
	}
	|NUMBER
	{
		addLit($1);
	}
;
Debut:
	PTOK CNFTOK NUMBER NUMBER
	{
		init($3, $4);
	}
;

%%

int yyerror(char *s) {
	reportError(s);
}

int parseCNF(FILE* f) {
  cnfin = f;
  return yyparse();
}
