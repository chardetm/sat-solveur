%{

#include "global.h"
#include <stdio.h>
#include <stdlib.h>

extern int yyparse();
extern FILE* conin;

extern void addLitCON(int ega);
extern void addAndCON();
extern void addOrCON();
extern void addImplCON();
extern void addNegCON();
extern void addArite();
extern void prepArite();

%}

%token LPAR RPAR LAND LOR LIMPL NEG NUMBER CONTOK EGA VAR INEGA FUNC VIRG

%precedence LPAR RPAR
%right LIMPL
%left LAND
%left LOR
%precedence NEG

%start File
%%

File:
	CONTOK FormuleEga
;

FormuleEga:
	LPAR FormuleEga RPAR
	|FormuleEga LAND FormuleEga
	{
		addAndCON();
	}
	|FormuleEga LOR FormuleEga
	{
		addOrCON();
	}
	|FormuleEga LIMPL FormuleEga
	{
		addImplCON();
	}
	|NEG FormuleEga
	{
		addNegCON();
	}
	|Terme EGA Terme
	{
		addLitCON(1);
	}
	|Terme INEGA Terme
	{
		addLitCON(0);
	}
;


Terme:
  LPAR Terme RPAR
  | VAR NUMBER
  {
  	addVar($2);
  }
  | FUNC LPAR Arg RPAR
  {
  	addFun($1);
  }
;

Arg:
  Arg VIRG Terme
  {
  	addArite();
  }
  |Terme
  {
  	prepArite();
  }
;

%%

int yyerror(char *s) {
}

int parseCON(FILE* f) {
  conin = f;
  return yyparse();
}
