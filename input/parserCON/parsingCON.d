module input.parsers.con;

import types.clause, types.valuation, types.instance, types.basic_types, types.graphe, types.arbre;
import std.stdio, std.string;
import std.exception, std.container, std.conv, std.math;

version(GNU) {
	import compiler_fixes.string;
}

private Arbre[] pile;
private var_t nbVariable;
private ulong[] arite;

private bool error;

extern(C)
{
	int parseCON(FILE* f);

	void addLitCON(int ega)
	{
		Arbre a = new Arbre("=");
		a.addFils(pile[$-1]);
		a.addFils(pile[$-2]);
		pile.length--;
		if(ega != 0)
			pile[$-1] = a;
		else
		{
			Arbre t = new Arbre("~");
			t.addFils(a);
			pile[$-1] = t;
		}
	}
	void addNegCON()
	{
		Arbre fils = new Arbre("~");
		fils.addFils(pile[$-1]);
		pile[$-1] = fils;
	}
	void addAndCON()
	{
		Arbre fils = new Arbre("/\\\\");
		fils.addFils(pile[$-2]);
		fils.addFils(pile[$-1]);
		pile.length--;
		pile[$-1] = fils;
	}
	void addOrCON()
	{
		Arbre fils = new Arbre("\\\\/");
		fils.addFils(pile[$-2]);
		fils.addFils(pile[$-1]);
		pile.length--;
		pile[$-1] = fils;
	}
	void addImplCON()
	{
		Arbre fils = new Arbre("\\\\/");
		Arbre fils1 = new Arbre("~");
		fils1.addFils(pile[$-2]);
		fils.addFils(fils1);
		fils.addFils(pile[$-1]);
		pile.length--;
		pile[$-1] = fils;
	}
	void addArite()
	{
		arite[$-1]++;
	}
	void prepArite()
	{
		arite ~= 1;
	}
	void addVar(char* sv)
	{
		string s = recupNumber(cast(string)fromStringz(sv));
		Arbre a = new Arbre("x");
		Arbre fils = new Arbre(s);
		a.addFils(fils);
		pile ~= a;
	}
	void addFun(char* fun)
	{
		Arbre a = new Arbre(recupFunName(cast(string)fromStringz(fun)));
		ulong n = arite[$-1];
		arite.length--;

		for(ulong i = n; i >= 1; i--)
		{
			a.addFils(pile[$-i]);
		}
		pile.length -= (n-1);
		pile[$-1] = a;
	}

}

Arbre parsingCon(ref File file)
{
	error = false;
	nbVariable = 0;
	pile.length = 0;
	parseCON(file.getFP());

	if(error)
	{
		throw new ParsingException("Parsing error");
	}
	
	return pile[0];
}

string recupFunName(string s)
{
	int i = 0;
	while(i < s.length && s[i] != '(')
		i++;
	return s[0..i];
}
string recupNumber(string s)
{
	int deb = 0;
	int fin = 0;
	while(deb < s.length && !isDigit(s[deb]))
		deb++;
	fin = deb;
	while(fin < s.length && isDigit(s[fin]))
		fin++;
	return s[deb..fin];
}

bool isDigit(char c)
{
	switch(c)
	{
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			return true;
		default:
			return false;
	}
}