%{

#include "global.h"
#include "con.tab.h"

#include <stdlib.h>
#include <stdio.h>

%}
%option noyywrap

%x NOTINIT

blanks      [ \t\r\n]+

digit       [0-9]
nzdigit     [1-9]
number      {digit}*{nzdigit}+{digit}*
fun [a-wy-zA-WY-Z][a-zA-Z]*

%%

<*>{number} {
	conlval=yytext;
	return NUMBER;
}
<NOTINIT>{fun} {
	conlval=yytext;
	return FUNC;
}

<INITIAL>"con" {
	BEGIN(NOTINIT);
	return CONTOK;
}

<*>"(" { return LPAR;}
<*>")" { return RPAR;}
<*>"/\\" { return LAND;}
<*>"\\/" {return LOR;}
<*>"=>" {return LIMPL;}
<*>"~" {return NEG;}
<*>"=" {return EGA;}
<*>"x" {return VAR;}
<*>"!=" {return INEGA;}
<*>"," {return VIRG;}

<*>{blanks} {}

. ;


%%
