%{

#include "global.h"
#include "dif.tab.h"

#include <stdlib.h>
#include <stdio.h>

%}
%option noyywrap

blanks      [ \t\r\n]+

digit       [0-9]
number [\+-]?{digit}+

%%

{number} {
	diflval=atoi(yytext);
	return NUMBER;
}
"dif" {return DIFTOK;}

"(" { return LPAR;}
")" { return RPAR;}
"/\\" { return LAND;}
"\\/" {return LOR;}
"=>" {return LIMPL;}
"~" {return NEG;}
"=" {return EGA;}
"x" {return VAR;}
"!=" {return INEGA;}
"<=" {return LEQ;}
"<" {return LESS;}
">=" {return GEQ;}
">" {return GREATER;}
"-" {return MINUS;}

{blanks} {}

. ;
%%
