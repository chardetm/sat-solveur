%{

#include "global.h"
#include <stdio.h>
#include <stdlib.h>

extern int yyparse();
extern FILE* difin;

extern void addLitDIF(long long v1, long long v2, int ega);
extern void addNegDIF();
extern void addAndDIF();
extern void addOrDIF();
extern void addImplDIF();
extern void addLeq(long long v1, long long v2, long long n);
extern void addGreater(long long v1, long long v2, long long n);
extern void afficher(char* s);
%}

%token LPAR RPAR LAND LOR LIMPL NEG NUMBER DIFTOK EGA VAR INEGA GREATER GEQ LESS LEQ MINUS

%precedence LPAR RPAR
%right LIMPL
%left LAND
%left LOR
%precedence NEG

%start File
%%

File:
	|DIFTOK FormuleDif
;
FormuleDif:
	LPAR FormuleDif RPAR
	|FormuleDif LAND FormuleDif
	{
		addAndDIF();
	}
	|FormuleDif LOR FormuleDif
	{
		addOrDIF();
	}
	|FormuleDif LIMPL FormuleDif
	{
		addImplDIF();
	}
	|NEG FormuleDif
	{
		addNegDIF();
	}
	|Atome
;

Atome:
	VAR NUMBER EGA VAR NUMBER
	{
		addLitDIF($2, $5, 1);
	}
	|VAR NUMBER INEGA VAR NUMBER
	{
		addLitDIF($2, $5, 0);
	}
	|VAR NUMBER LEQ VAR NUMBER
	{
		addLeq($2,$5,0);
	}
	|VAR NUMBER MINUS VAR NUMBER LEQ NUMBER
	{
		addLeq($2,$5,$7);
	}
	|VAR NUMBER LEQ NUMBER
	{
		addLeq($2,0,$4);
	}
	|VAR NUMBER LESS VAR NUMBER
	{
		addLeq($2,$5,-1);
	}
	|VAR NUMBER MINUS VAR NUMBER LESS NUMBER
	{
		addLeq($2,$5,$7 -1);
	}
	|VAR NUMBER LESS NUMBER
	{
		addLeq($2,0,$4 -1);
	}
	|VAR NUMBER GREATER VAR NUMBER
	{
		addGreater($2,$5,0);
	}
	|VAR NUMBER MINUS VAR NUMBER GREATER NUMBER
	{
		addGreater($2,$5,$7);
	}
	|VAR NUMBER GREATER NUMBER
	{
		addGreater($2,0,$4);
	}
	|VAR NUMBER GEQ VAR NUMBER
	{
		addGreater($2,$5,1);
	}
	|VAR NUMBER MINUS VAR NUMBER GEQ NUMBER
	{
		addGreater($2,$5,$7-1);
	}
	|VAR NUMBER GEQ NUMBER
	{
		addGreater($2,0,$4-1);
	}
;

%%

int yyerror(char *s) {
	afficher(s);
}

int parseDIF(FILE* f) {
  difin = f;
  return yyparse();
}
