module input.parsers.dif;

import types.clause, types.valuation, types.instance, types.basic_types, types.graphe, types.arbre;
import std.stdio, std.string;
import std.exception, std.container, std.conv, std.math;

private Arbre[] pile;
private var_t nbVariable;

private bool error;

extern(C)
{
	int parseDIF(FILE* f);

	void addLitDIF(long l1, long l2, int ega)
	{
		if(ega)
		{
			addLeq(l1,l2,0);
			addGreater(l1,l2,-1);
			Arbre fils = new Arbre("/\\\\");
			fils.addFils(pile[$-2]);
			fils.addFils(pile[$-1]);
			pile.length--;
			pile[$-1]=fils;
		}
		else
		{
			addLeq(l1,l2,-1);
			addGreater(l1,l2,0);
			Arbre fils = new Arbre("\\\\/");
			fils.addFils(pile[$-2]);
			fils.addFils(pile[$-1]);
			pile.length--;
			pile[$-1]=fils;

		}
	}
	void addNegDIF()
	{
		Arbre fils = new Arbre("~");
		fils.addFils(pile[$-1]);
		pile[$-1] = fils;
	}
	void addAndDIF()
	{
		Arbre fils = new Arbre("/\\\\");
		fils.addFils(pile[$-2]);
		fils.addFils(pile[$-1]);
		pile.length--;
		pile[$-1] = fils;
	}
	void addOrDIF()
	{
		Arbre fils = new Arbre("\\\\/");
		fils.addFils(pile[$-2]);
		fils.addFils(pile[$-1]);
		pile.length--;
		pile[$-1] = fils;
	}
	void addImplDIF()
	{
		Arbre fils = new Arbre("\\\\/");
		Arbre fils1 = new Arbre("~");
		fils1.addFils(pile[$-2]);
		fils.addFils(fils1);
		fils.addFils(pile[$-1]);
		pile.length--;
		pile[$-1] = fils;
	}
	void addLeq(long v1, long v2, long n)
	{
		if(v1 <= 0 || v2 < 0)
		{
			error = true;
		}
		Arbre a = new Arbre("<=");
		if(v2 == 0)
		{
			Arbre fils = new Arbre("x");
			Arbre fils1 = new Arbre(to!string(v1));
			fils.addFils(fils1);
			a.addFils(fils);
		}
		else
		{
			Arbre fils1 = new Arbre("-");
			Arbre fils11 = new Arbre("x");
			Arbre fils111 = new Arbre(to!string(v1));
			Arbre fils12 = new Arbre("x");
			Arbre fils121 = new Arbre(to!string(v2));
			fils11.addFils(fils111);
			fils12.addFils(fils121);
			fils1.addFils(fils11);
			fils1.addFils(fils12);
			a.addFils(fils1);
		}

		a.addFils(new Arbre(to!string(n)));
		pile ~= a;
	}
	void addGreater(long v1, long v2, long n)
	{
		addLeq(v1, v2, n);
		Arbre fils = new Arbre("~");
		fils.addFils(pile[$-1]);
		pile[$-1] = fils;
	}

}

Arbre parsingDif(ref File file)
{
	error = false;
	nbVariable = 0;
	pile.length = 0;
	parseDIF(file.getFP());

	if(error)
	{
		throw new ParsingException("Parsing error");
	}
	
	return pile[0];
}
