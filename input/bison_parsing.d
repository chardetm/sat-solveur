﻿module input.bison_parsing;

public import input.parsers.cnf, input.parsers.ega, input.parsers.dif, input.parsers.con, input.parsers.tsei, input.parsers.col;
import std.stdio, std.string;
version(GNU) {
	import compiler_fixes.string;
}

extern(C)
{
	void afficher(char* s)
	{
		writeln(cast(string)fromStringz(s));
	}
}