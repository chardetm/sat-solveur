%{

#include "global.h"
#include <stdio.h>
#include <stdlib.h>

extern int yyparse();
extern FILE* tseiin;

extern void addLitTSEI(long long l);
extern void addNegTSEI();
extern void addAndTSEI();
extern void addOrTSEI();
extern void addImplTSEI();

%}

%token LPAR RPAR LAND LOR LIMPL NEG NUMBER TSEITOK

%precedence LPAR RPAR
%right LIMPL
%left LAND
%left LOR
%precedence NEG

%start File
%%

File:
	TSEITOK Formule
;

Formule:
	LPAR Formule RPAR
	| NEG Formule
	{
		addNegTSEI();
	}
	| Formule LAND Formule
	{
		addAndTSEI();
	}
	| Formule LOR Formule
	{
		addOrTSEI();
	}
	| Formule LIMPL Formule
	{
		addImplTSEI();
	}
	| NUMBER
	{
		addLitTSEI($1);
	}
;

%%

int yyerror(char *s) {
}

int parseTSEI(FILE* f) {
  tseiin = f;
  return yyparse();
}
