module input.parsers.tsei;

import types.clause, types.valuation, types.instance, types.basic_types, types.graphe, types.arbre;
import std.stdio, std.string;
import std.exception, std.container, std.conv, std.math;

private Arbre[] pile;
private var_t nbVariable;

private bool error;

extern(C)
{
	int parseTSEI(FILE* f);

	void addLitTSEI(long l)
	{
		pile ~= new Arbre(to!string(l));
	}
	void addNegTSEI()
	{
		Arbre fils = new Arbre("~");
		fils.addFils(pile[$-1]);
		pile[$-1] = fils;
	}
	void addAndTSEI()
	{
		Arbre fils = new Arbre("/\\\\");
		fils.addFils(pile[$-2]);
		fils.addFils(pile[$-1]);
		pile.length--;
		pile[$-1] = fils;
	}
	void addOrTSEI()
	{
		Arbre fils = new Arbre("\\\\/");
		fils.addFils(pile[$-2]);
		fils.addFils(pile[$-1]);
		pile.length--;
		pile[$-1] = fils;
	}
	void addImplTSEI()
	{
		Arbre fils = new Arbre("\\\\/");
		Arbre fils1 = new Arbre("~");
		fils1.addFils(pile[$-2]);
		fils.addFils(fils1);
		fils.addFils(pile[$-1]);
		pile.length--;
		pile[$-1] = fils;
	}

}

Arbre parsingTsei(ref File file)
{
	error = false;
	nbVariable = 0;
	pile.length = 0;
	parseTSEI(file.getFP());

	if(error)
	{
		throw new ParsingException("Parsing error");
	}
	
	return pile[0];
}
