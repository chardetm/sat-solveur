%{

#include "global.h"
#include "tsei.tab.h"

#include <stdlib.h>
#include <stdio.h>

%}
%option noyywrap

blanks      [ \t\r\n]+

digit       [0-9]
nzdigit     [1-9]
number      [\+-]?{digit}*{nzdigit}+{digit}*

%%

"tsei" {
	return TSEITOK;
}
{number} {
	tseilval=atoi(yytext);
	return NUMBER;
}

"(" { return LPAR;}
")" { return RPAR;}
"/\\" { return LAND;}
"\\/" {return LOR;}
"=>" {return LIMPL;}
"~" {return NEG;}

{blanks} {}

. ;


%%
