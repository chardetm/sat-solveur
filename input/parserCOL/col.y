%{

#include "global.h"
#include <stdio.h>
#include <stdlib.h>

extern int yyparse();
extern FILE* colin;

void reportErrorCol(char* s);
void initCol(long long v, long long c);
void addEdge(long long v1, long long v2);

%}

%token PTOK EDGETOK NUMBER ERROR DEBEDGE

%start Edges
%%

Edges:
	Edges DEBEDGE NUMBER NUMBER
	{
		addEdge($3, $4);
	}
	|Head
	|ERROR
	{
		reportErrorCol("");
	}
;
Head:
	PTOK EDGETOK NUMBER NUMBER
	{
		initCol($3, $4);
	}
	
	

%%

int yyerror(char *s) {
}

int parseCOL(FILE* f) {
  colin = f;
  return yyparse();
}
