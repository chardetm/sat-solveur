%{

#include "global.h"
#include "col.tab.h"

#include <stdlib.h>
#include <stdio.h>

%}
%option noyywrap

blanks      [ \t\r\n]+

digit       [0-9]
nzdigit     [1-9]
number      [\+-]?{digit}*{nzdigit}+{digit}*

subcomment  [\ {digit}\+-].*
comment     ^c{subcomment}?$

deberror	^[^pe]

%%

"p" {return PTOK;}
"edge" {return EDGETOK;}
"e" {return DEBEDGE;}

{number} {
	collval=atoi(yytext);
	return NUMBER;
}

{blanks} {}
{comment} {}
{deberror} {return ERROR;}

. ;


%%
