module input.parsers.col;

import types.clause, types.valuation, types.instance, types.basic_types, types.graphe, types.arbre;
import std.stdio, std.string;
import std.exception, std.container, std.conv, std.math;

private Graphe g;
private uint c, realc = 0;
private var_t v, realv = 0;

private bool error;

extern(C)
{
	int parseCOL(FILE* f);
	void initCol(long nbV, long nbE)
	{
		c = cast(uint)nbE;
		v = cast(var_t)nbV;
	}
	void addEdge(long v1, long v2)
	{
		realc++;
		realv = v1 > realv ? v1 : realv;
		realv = v2 > realv ? v2 : realv;
		g.addEdge(v1,v2);
	}
	void reportErrorCol(char* s)
	{
		error = true;
	}
	
}

Graphe parsingCol(ref File file)
{
	error=false;
	c = 0;
	realc = 0;
	v = 0;
	realv = 0;
	g = new Graphe();

	parseCOL(file.getFP());
	
	if (error) {
		throw new ParsingException("Parsing error");
	}

	if (realc != c) {
		stderr.writeln("Warning: wrong number of edges in the header (", realc, " edges instead of ", c, ")");
		c =realc;
	}
	if (realv != v) {
		stderr.writeln("Warning: wrong number of vertices in the header (", realv, " vertices instead of ", v, ")");
		v = realv;
	}
	
	return g;
}