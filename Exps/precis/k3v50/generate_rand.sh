#!/bin/bash


	mkdir random_tests 2> /dev/null
	for k in 3
	do
		for numvars in 50
		do
			for numclauses in $(seq 5 5 1000)
			do
				for numsample in $(seq 1 10)
				do
					echo $k $numvars $numclauses N random_tests/rand-k$k-nv$numvars-nc$numclauses-$numsample.cnf
					#python random_ksat.py $k $numvars $numclauses N random_tests/rand-k$k-nv$numvars-nc$numclauses-$numsample.cnf
					./genksat $k $numvars $numclauses > random_tests/rand-k$k-nv$numvars-nc$numclauses-$numsample.cnf
				done
			done
		done
	done