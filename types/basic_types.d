module types.basic_types;

import std.exception;
import types.clause;

alias lit_t = long;
alias var_t = ulong;
alias etage_t = ulong;
alias ver_t = ulong;
alias valth_t = long;

enum valu_t {Inconnu, Vrai, Faux};
enum mode_t {NORMAL, CL, CL_INTERAC, COLOR, EXPLAIN};
enum format_t {CNF, OTHER, EGA, CON, DIF};
enum pari_t {NORMAL, RAND, MOMS, DLIS, VSIDS}

alias Vrai = valu_t.Vrai;
alias Faux = valu_t.Faux;
alias Inconnu = valu_t.Inconnu;

class NotSolvable : Exception 
{
	this(string s) 
	{
		super(s);
	}
}

class ParsingException : Exception
{
	this(string s) 
	{
		super(s);
	}
}
