module types.arbre;

import std.string, std.file, std.stdio, std.process, std.conv, std.math, std.ascii;
import types.clause, types.instance;

class Arbre
{
protected:
	Arbre[] _fils;
	string _etiquette;
	var_t _variableAssoc;
	static int[] _variableUse = new int[](0);
	static int cAnd = 0;
	static int cOr = 0;
	static int cEga = 0;
	static int cNeg = 0;
	static int cVar = 0;
	static int cMinus = 0;
	static var_t nexDispo = 0;
	static int cLeq = 0;

public:
	this()
	{
		this("");
	}
	this(string etiquette)
	{
		_etiquette = etiquette;
		_fils.length = 0;
	}
	void addFils(Arbre a)
	{
		_fils ~= a;
	}

	string etiquette()
	{
		return _etiquette;
	}
	void etiquette(string et)
	{
		_etiquette = et;
	}

	Arbre fils(ulong i = 0)
	{
		return _fils[i];
	}
	Arbre[] lFils()
	{
		return _fils;
	}

	ulong length()
	{
		return _fils.length;
	}

	void toPDF(string fileName = "arbre")
	{
		initNbVariable(entierMax());
		File f = File(fileName~".dot", "w");
		f.writeln("digraph G {");
		f.writeln("size = \"4,4\";");
		f.writeln("\"" ~_etiquette ~ "_0\";\n");
		f.writeln(toDot());
		f.writeln("}");
		foreach(i;0.._variableUse.length)
		{
			_variableUse[i] = 0;
		}
		cNeg = 0;
		cAnd = 0;
		cOr = 0;
		cMinus = 0;
		cEga = 0;
		cVar = 0;
		cLeq = 0;
		executeShell("dot -Tpdf " ~ fileName ~ ".dot -o " ~ fileName ~ ".pdf");
		executeShell("rm " ~ fileName ~".dot");
	}

	string toDot(string etiPere = "")
	{
		string etiquette;
		string str;
		if(_etiquette == "\\\\/")
		{
			etiquette = _etiquette~"_"~to!string(cOr);
			cOr++;
		}
		else if(_etiquette == "/\\\\")
		{
			etiquette = _etiquette~"_" ~ to!string(cAnd);
			cAnd++;
		}
		else if(_etiquette == "~")
		{
			etiquette = _etiquette~"_" ~ to!string(cNeg);
			cNeg++;
		}
		else if(_etiquette == "=")
		{
			etiquette = _etiquette~"_"~to!string(cEga);
			cEga++;
		}
		else if(_etiquette == "x")
		{
			etiquette = _etiquette~"_"~to!string(cVar);
			cVar++;
		}
		else if(_etiquette == "<=")
		{
			etiquette = _etiquette~"_"~to!string(cLeq);
			cLeq++;
		}
		else if(_etiquette == "-")
		{
			etiquette = _etiquette~"_"~to!string(cMinus);
			cMinus++;
		}
		else if(!isDigit(_etiquette[0]) && _etiquette[0] != '-')
		{
			etiquette = _etiquette;
		}
		else
		{
			long var = to!long(_etiquette);
			etiquette = _etiquette~"_"~to!string(_variableUse[abs(var)]);
			_variableUse[abs(var)]++;
		}
		if(etiPere != "")
		{
			str ~= "\"" ~ etiPere ~ "\"->\"" ~ etiquette ~ "\"\n";
		}
		foreach(e;_fils)
		{
			str ~= e.toDot(etiquette);
		}
		return str;
	}

	static void initNbVariable(ulong a)
	{
		_variableUse.length = a+1;
		foreach(i;0.._variableUse.length)
		{
			_variableUse[i] = 0;
		}
	}

	Instance transfoTseitin(pari_t choix, mode_t mode = mode_t.NORMAL)
	{
		nexDispo = entierMax();
		var_t V = getRealV();
		Instance res = new Instance(mode, choix);
		Clause[] clauseSet = transfoTseitinSousFor();
		Clause actuel = new Clause();
		actuel.add_literal(_variableAssoc);
		clauseSet ~= actuel;
		res.init(V, cast(uint)clauseSet.length);
		foreach(e; clauseSet)
		{
			res.addClause(e);
		}
		Clause.init(res._val);

		return res;
	}

	Clause[] transfoTseitinSousFor()
	{
		Clause[] res;
		foreach(e;_fils)
		{
			res ~= e.transfoTseitinSousFor();
		}
		if(_etiquette == "\\\\/")
		{
			Clause c1 = new Clause();
			c1.add_literal(-_variableAssoc);
			c1.add_literal(_fils[0]._variableAssoc);
			c1.add_literal(_fils[1]._variableAssoc);

			Clause c2 = new Clause();
			c2.add_literal(_variableAssoc);
			c2.add_literal(-_fils[0]._variableAssoc);

			Clause c3 = new Clause();
			c3.add_literal(_variableAssoc);
			c3.add_literal(-_fils[1]._variableAssoc);
			res ~= c1;
			res ~= c2;
			res ~= c3;
		}
		else if(_etiquette == "/\\\\")
		{
			Clause c1 = new Clause();
			c1.add_literal(_variableAssoc);
			c1.add_literal(-_fils[0]._variableAssoc);
			c1.add_literal(-_fils[1]._variableAssoc);

			Clause c2 = new Clause();
			c2.add_literal(-_variableAssoc);
			c2.add_literal(_fils[0]._variableAssoc);

			Clause c3 = new Clause();
			c3.add_literal(-_variableAssoc);
			c3.add_literal(_fils[1]._variableAssoc);
			res ~= c1;
			res ~= c2;
			res ~= c3;
		}
		else if(_etiquette == "~")
		{
			Clause c1 = new Clause();
			c1.add_literal(-_variableAssoc);
			c1.add_literal(-_fils[0]._variableAssoc);

			Clause c2 = new Clause();
			c2.add_literal(_variableAssoc);
			c2.add_literal(_fils[0]._variableAssoc);

			res ~= c1;
			res ~= c2;
		}
		else
		{
			Clause c1 = new Clause();
			c1.add_literal(-_variableAssoc);
			c1.add_literal(to!ulong(_etiquette));

			Clause c2 = new Clause();
			c2.add_literal(_variableAssoc);
			c2.add_literal(-to!ulong(_etiquette));

			res ~= c1;
			res ~= c2;
		}
		return res;
	}
	int taille()
	{
		int res = 1;
		foreach(e;_fils)
		{
			res += e.taille();
		}
		return res;
	}

	var_t varMax()
	{
		var_t res = 0;
		if(_etiquette == "x")
		{
			return to!ulong(_fils[0].etiquette);
		}
		else if(_fils.length > 0)
		{
			foreach(e;_fils)
			{
				var_t repFils = e.varMax();
				res = repFils > res ? repFils : res;
			}
		}

		return res;
	}

	var_t entierMax()
	{
		var_t res  = 0;
		if(_fils.length == 0)
			return abs(to!long(_etiquette));
		foreach(e;_fils)
		{
			var_t repFils = e.entierMax();
			res = repFils > res ? repFils : res;
		}
		return res;
	}
	var_t getRealV()
	{
		foreach(e;_fils)
		{
			e.getRealV();
		}
		nexDispo++;
		_variableAssoc = nexDispo;
		return _variableAssoc;
	}

	override bool opEquals(Object o)
	{
		if(typeid(this) == typeid(o))
		{
			return egal(cast(Arbre) o);
		}
		else
			return false;
	}

	bool egal(Arbre a)
	{
		bool rep = a.etiquette == _etiquette && a._fils.length == _fils.length;
		foreach(i;0.._fils.length)
		{
			if(!rep)
				break;
			rep = _fils[i].egal(a.fils(i));
		}
		return rep;
	}
}