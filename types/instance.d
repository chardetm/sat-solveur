module types.instance;

import std.stdio, std.string;
import std.exception, std.container, std.conv, std.math;
import types.valuation, types.clause, input.bison_parsing, types.arbre;

class Instance
{
	public Valuation _val;
	protected Clause[] _clauses;
	protected uint _C;
	protected mode_t _mode;

	deprecated this () { // On fait quoi de ça ?
		_val = new Valuation();
		_mode = mode_t.NORMAL;
	}

	this (mode_t mode, pari_t choix) {
		_val = new Valuation(choix);
		_mode = mode;
	}

	this (ref File f, mode_t mode, pari_t choix) {
		this(bisonParsing(f, mode, choix)); // Call copy constructor
	}

	this (Instance f) { // Copy constructor
		_val = f._val;
		_mode = f._mode;
		_C = f._C;
		_clauses.length = f._clauses.length;
		for (int i = 0; i <_clauses.length; ++i) {
			_clauses[i] = f._clauses[i];
		}
	}

	void init(var_t V, uint C)
	{
		this._C = _C;
		_val.init(V);
	}

	Clause getClause(int i)
	{
		return _clauses[i];
	}

	@property ulong clauseLength()
	{
		return _clauses.length;
	}
	Clause[] getClauses()
	{
		return _clauses;
	}

	void addClause(Clause c)
	{
		_clauses ~= c;
	}

	@property override string toString()
	{
		string str = _val.toString;
		str ~= "\n";
		str ~= to!string(_clauses.length) ~ "\n";
		foreach(e;_clauses)
		{
			str ~= e.toString ~ "\n";
		}
		return str;
	}

	@property bool isTrue()
	{
		bool res = true;
		foreach(e;_clauses)
		{
			res = e.isSatisfied;
			if(!res)
				break;
		}
		return res;
	}

	@property mode_t mode () {
		return _mode;
	}
	
	@property Valuation val () {
		return _val;
	}

	void setVal(Valuation val)
	{
		_val = val;
	}
	void setClauses(Clause[] clause)
	{
		_clauses = clause;
	}
	 void toCNFFile(string filename = "test.cnf")
	 {
	 	File f = File(filename, "w");
	 	f.writeln("p cnf " ~ to!string(_val.length) ~ " " ~ to!string(_clauses.length));
	 	foreach(e;_clauses)
	 	{
	 		f.writeln(e);
	 	}
	 }

	 Arbre toFormule()
	 {
	 	Arbre a;
	 	if(_clauses.length == 0)
	 		return a;
	 	a = _clauses[0].toArbre();
	 	if(a is null)
	 		return null;
	 	foreach(i;1.._clauses.length)
	 	{
	 		Arbre clause = _clauses[i].toArbre();
	 		if(clause is null)
	 			return null;
	 		Arbre te = new Arbre("/\\\\");
	 		te.addFils(a);
	 		te.addFils(clause);
	 		a = te;
	 	}

	 	return a;
	 }
}
