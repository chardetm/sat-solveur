module types.grapheDif;

import std.string, std.file, std.stdio, std.process, std.conv, std.math;
import types.basic_types, types.entier;

class GrapheDif
{
protected:
	// Entier est un type long auquel on a rajouté l'infini
	ulong _varMax;
	bool[][] _matrAdj;
	Entier[][] _matrPoid;

public:
	this(ulong varMax)
	{
		_varMax = varMax;
		_matrAdj.length = _varMax+1;
		_matrPoid.length = _varMax +1;
		for(long i= 1; i <= _varMax; i++)
		{
			_matrPoid[i].length = _varMax+1;
			_matrAdj[i].length = _varMax + 1;
			_matrAdj[i][] = false;
		}
	}

	this(GrapheDif g)
	{
		_varMax = g._varMax;
		_matrAdj.length = _varMax+1;
		_matrPoid.length = _varMax +1;
		for(long i= 1; i <= _varMax; i++)
		{
			_matrPoid[i].length = _varMax+1;
			_matrAdj[i].length = _varMax + 1;
			for(int j = 1; j <= _varMax; j++)
			{
				_matrAdj[i][j] = g._matrAdj[i][j];
				_matrPoid[i][j] = g._matrPoid[i][j];
			}
		}
	}

	void addArrete(var_t a1, var_t a2, Entier poid)
	{
		debug(2)
		{
			writeln("L'arrête ", a1,"->",a2, " de poids ", poid, " est ajoutée");
		}
		if(_matrAdj[a1][a2] && _matrPoid[a1][a2] > poid)
		{
			//on a une information qui rajoute une contraite : on change donc l'arrête.
			_matrPoid[a1][a2] = poid;
		}
		else if(!_matrAdj[a1][a2])
		{
			_matrAdj[a1][a2] = true;
			_matrPoid[a1][a2] = poid;
		}
	}
	void addArrete(var_t a1, var_t a2, long poid)
	{
		debug(2)
		{
			writeln("L'arrête ", a1,"->",a2, " de poids ", poid, " est ajoutée");
		}
		if(_matrAdj[a1][a2] && _matrPoid[a1][a2] > poid)
		{
			//on a une information qui rajoute une contraite : on change donc l'arrête.
			_matrPoid[a1][a2] = poid;
		}
		else if(!_matrAdj[a1][a2])
		{
			_matrAdj[a1][a2] = true;
			_matrPoid[a1][a2] = poid;
		}
	}
	bool arretePres(var_t a1, var_t a2)
	{
		return _matrAdj[a1][a2];
	}
	Entier arretePoid(var_t a1, var_t a2)
	{
		return _matrPoid[a1][a2];
	}

	bool cycleAbsorbant()//bellman-ford pour l'algorithme, rien à rajouté
	{
		debug(2)
		{
			writeln("Liste arrête");
			for(long u = 1;u <= _varMax; u++)
			{
				for(long v = 1; v <= _varMax; v++)
				{
					if(_matrAdj[u][v])
						writeln(u, "->", v);
				}
			}
		}
		//init
		Entier[][] poids;
		long[][] nouvPoids;
		Entier[][] prec;
		poids.length = _varMax +1;
		nouvPoids.length = _varMax +1;
		prec.length = _varMax +1;
		for(long i = 1; i <= _varMax; i++)
		{
			poids[i].length = _varMax + 1;
			poids[i][] = Entier(1, true);
			poids[i][i] = Entier(0, false);
			nouvPoids[i].length = _varMax + 1;
			nouvPoids[i][] =-1;
			nouvPoids[i][i] = 0;
			prec[i].length = _varMax + 1;
			prec[i][] = Entier(1, true);
		}
	
		//comme notre graphe n'est pas connexe, on doit vérifier pour toutes les origines
		for(long s = 1; s <= _varMax; s++)//s origine du chemin
		{
			for(long i = 1; i < _varMax; i++)
			{
				bool nonChange = true;
				for(long u = 1; u <= _varMax; u++)
				{
					if(nouvPoids[s][u] != i-1)
						continue;
					for(long v = 1; v <= _varMax; v++)
					{
						if(_matrAdj[u][v])
						{
							if(u==v)
							{
								if(_matrPoid[u][v] < 0)
								{
									return true;
								}
							}
							else
							{
								Entier nouveauPoids = poids[s][u] + _matrPoid[u][v];
								if(nouveauPoids < poids[s][v])
								{
									prec[s][v] = u;
									poids[s][v] = nouveauPoids;
									nouvPoids[s][v] = i;
									nonChange = false;
								}
							}
						}
					}
				}
				if(nonChange)
					break;
			}
			for(long u = 1;u <= _varMax; u++)
			{
				for(long v = 1; v <= _varMax; v++)
				{
					if(_matrAdj[u][v])
					{
						if(poids[s][u] + _matrPoid[u][v] < poids[s][v])
						{
							debug(2)
							{
								writeln("origin : ", s, " " ,u, "->", v, " : ", poids[s][u], " + ", _matrPoid[u][v], " < ", poids[s][v]);
							}
							return true;
						}
					}
				}
			}
		}
		return false; //pas de cycle absorbant
	}
	void affichageArrete()
	{
		writeln("Liste arrête");
		for(long u = 1;u <= _varMax; u++)
		{
			for(long v = 1; v <= _varMax; v++)
			{
				if(_matrAdj[u][v])
					writeln(u, "->", v);
			}
		}		
	}

}
