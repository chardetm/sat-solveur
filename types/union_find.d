module types.union_find;

import types.basic_types;

class UnionFind
{
protected:
	var_t[] _representant;
	size_t[] _taille;

public:
	this(var_t nbVar)
	{
		_representant.length = nbVar+1;
		_taille.length = nbVar+1;
		foreach(i;1..nbVar+1)
		{
			_representant[i] = i;
			_taille[i] = 1;
		}
	}
	this(UnionFind u)
	{
		_representant = u._representant.dup;
		_taille = u._taille.dup;
	}

	var_t find(var_t x)
	{
		if(_representant[x] != x)
		{
			_representant[x] = find(_representant[x]);
		}
		return _representant[x];
	}

	var_t unionF(var_t x1, var_t x2)
	{
		var_t r1, r2;
		r1 = find(x1);
		r2 = find(x2);
		if (_taille[r1] <= _taille[r2]) {
			_representant[r1] = r2;
			_taille[r2] += _taille[r1];
			return r2;
		} else {
			_representant[r2] = r1;
			_taille[r1] += _taille[r2];
			return r1;
		}
	}
}
