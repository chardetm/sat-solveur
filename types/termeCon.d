module types.termeCon;

import types.basic_types, std.stdio, types.union_find, std.conv;

class TermeCongruence
{
protected:
	string _name;
	ulong _arite;
	TermeCongruence[] _arg;

public:
	this()
	{
		_name = "";
		_arite = 0;
	}
	this(string name, ulong arite)
	{
		_name = name;
		_arite = arite;
	}
	this(TermeCongruence t)
	{
		_name = t.name;
		_arite = t.arite;
		foreach(i;0.._arite)
		{
			_arg ~= new TermeCongruence(t.arg(i));
		}
	}

	void addArg(TermeCongruence t)
	{
		_arg ~= t;
	}

	TermeCongruence arg(ulong n)
	{
		return _arg[n];
	}

	string name()
	{
		return _name;
	}
	ulong arite()
	{
		return _arite;
	}

	bool egal(TermeCongruence t)//egalité stricte, qui ne compte pas compte des variables
	{
		if(_name != t.name || _arite != t.arite)
			return false;
		foreach(i;0.._arite)
		{
			if(!_arg[i].egal(t.arg(i)))
				return false;
		}
		return true;
	}

	//egalité qui prend compte des variables (ie, si x1 = x2, g(x1) = g(x2))
	bool egal(TermeCongruence t, UnionFind u, TermeCongruence[] indexToTerme, ulong varMax)
	{
		if(_name[0] == 'x')
		{
			if(t.name[0] == 'x')
			{
				//deux variables. On recupére leur numéro
				return u.find(to!var_t(_name[1..$])) == u.find(to!var_t(t.name[1..$]));
			}
			else
			{
				//on recupere l'indice du terme t
				int indT = 0;
				while(indexToTerme[indT] != t)
					indT++;
				//une variable n'est égale à un terme que si ils ont le même représentant
				return u.find(to!var_t(_name[1..$])) == u.find(indT+varMax+1);
			}
		}
		else
		{
			int ind = 0;
			while(!egal(indexToTerme[ind]))
				ind++;
			if(t.name[0] == 'x')
			{
				return u.find(ind+varMax+1) == u.find(to!var_t(t.name[1..$]));
			}
			else
			{
				//pas de variable
				//on récupere les termes correspondant
				//on recupere l'indice du terme t
				int indT = 0;
				while(indexToTerme[indT] != t)
					indT++;
				if(u.find(ind+varMax+1) == u.find(indT+varMax+1))
				{
					//on vérifie si ils ont le même représentant (si oui, alors ils sont égaux)
					return true;
				}
				bool rep = _name == t.name && _arite == t.arite;
				//deux fonctions ne sont égaux que s'ils ont le même nom et la même arité
				if(!rep)
				{
					return false;
				}
				//on sait que _arite = t.arite
				foreach(i;0.._arite)
				{
					//on continue par récurrence
					rep = rep && _arg[i].egal(t._arg[i], u, indexToTerme, varMax);
				}
				return rep;
			}
		}
	}

	override bool opEquals(Object o)
	{
		if(typeid(this) == typeid(o))
		{
			return egal(cast(TermeCongruence) o);
		}
		else
			return false;
	}

	override string toString()
	{
		assert(_arite == _arg.length);
		string str = _name;
		if (_arg.length >= 1) {
			str ~= ("(" ~ _arg[0].toString());
			foreach (e; _arg[1 .. $]) {
				str ~= (", " ~ e.toString());
			}
			str ~= ")";
		}
		return str;
	}

	string toString(in ref long[] val) { // On change les noms des variables en leur valeur que si le terme n'est pas une variable
		assert(_arite == _arg.length);
		string str = _name;
		if (_arg.length >= 1) {
			str ~= ("(" ~ _arg[0].toStringSub(val));
			foreach (e; _arg[1 .. $]) {
				str ~= ("," ~ e.toStringSub(val));
			}
			str ~= ")";
		}
		return str;
	}
	
	protected string toStringSub(in ref long[] val) { // Au moins au deuxième niveau de la hiérarchie : on remplace le nom des variables par leur valeur
		assert(_arite == _arg.length);
		string str;
		if (_arg.length >= 1) {
			str ~= _name;
			str ~= ("(" ~ _arg[0].toStringSub(val));
			foreach (e; _arg[1 .. $]) {
				str ~= ("," ~ e.toStringSub(val));
			}
			str ~= ")";
		} else { // Variable
			str ~= to!string(val[to!size_t(_name[1 .. $])]); // x73 -> val[73]
		}
		return str;
	}
}
