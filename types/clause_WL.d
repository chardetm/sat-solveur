module types.clause_WL;

public import types.basic_types;

import types.valuation;
import std.math, std.exception,std.conv, std.stdio, types.clause;

class ClauseWL : Clause
{
	private int _lit1=0;
	private int _lit2=1;

	this()
	{
		
	}

	this(Clause c)
	{
		_resolution1 = c.resolution1;
		_resolution2 = c.resolution2;
		_literals = c.getLiterals;
		_val = c.associatedValuation;
	}

	override @property bool isTautology()
	{
		bool res = false;
		int i;
		
		for(i=0; i < _literals.length-1 && !res; i++) {
			// Comme les litéraux sont triés par valeur absolue et non dupliqués,
			// si deux litéraux de suite ont la même valeur absolue, la clause est
			// une tautologie et inversement
			res = abs(_literals[i]) == abs(_literals[i+1]);
		}
		//on met nos jumelles sur les deux literaux opposés.
		//ca nous permet de vérifier, ensuite, si la clause est une tautologie (et donc inactive) en temps constant.
		_lit1 = i-1;
		_lit2 = i;

		return res;
	}

	override @property bool isMonome()
	{
		bool res = (_literals.length == 1);

		if(res) {
			//pour éviter des problèmes d'accès aux tableaux (du genre _literals[1])
			//on surveille que le literal qui existe.
			//on notera d'ailleurs qu'on a toujours, dans ce cas, _val.isTrue(_literals[0]) = true
			//(sinon une exception a été jeté)
			_lit2 = 0;
			_lit1 = 0;
		}

		return res;
	}

	@property int getLit1() const
	{
		return _lit1;
	}
	@property int getLit2() const
	{
		return _lit2;
	}
	@property bool isSatisfiedWL() const
	{
		if (_val.isTrue(_literals[_lit1]) || _val.isTrue(_literals[_lit2]))
			return true;
		return _literals[_lit1] == -_literals[_lit2];
	}
	@property bool updateWL()//return vrai si pas de déduction à fait, faux sinon.
	{
		if(isSatisfiedWL())
		{
			//isSatisfiedWL renvoie vrai si la clause est vraie (evidemment) ou si c'est une tautologie
			//comme updateWL est la seule fonction qui change les literaux surveillés, on a bien que
			//pour une tautologie, _lit1 et _lit2 ne sont jamais modifiés.
			return true;
		}
		if(_val.isFalse(_literals[_lit1]))//on doit changer _lit1 de place.
		{
			int i = 0;
			foreach(e; _literals)
			{
				if(!_val.isFalse(e) && i!= _lit2)
				{
					break;
				}
				i++;
			}
			if(i == _literals.length)
			{
				// à part _lit2, tous les literaux sont à faux.
				// on a pas pu changer _lit1 de place : déduction necessaire.
				return false;
			}
			_lit1 = i;
		}
		if(_val.isFalse(_literals[_lit2])) // On doit changer _lit2 de place
		{
			int i = 0;
			foreach(e;_literals)
			{
				if(!_val.isFalse(e) && i != _lit1)
				{
					break;
				}
				i++;
			}
			if(i == _literals.length)
			{
				// à part _lit1, tous les literaux sont à faux.
				// on a pas pu changer _lit2 de place : déduction necessaire.
				return false;
			}
			_lit2 = i;
		}
		return true;
	}

	@property bool deductionWL() // retourne vrai si pas de backtracking à faire, faux sinon
	{
		if(_val.isFalse(_literals[_lit1]))
		{
			if(_val.isFalse(_literals[_lit2]))
			{
				return false; // conflit -> backtracking
			}
			else if(_val.isUnknown(_literals[_lit2]))
			{
				_val.modif(abs(_literals[_lit2]), _literals[_lit2] > 0, this);
			}
		}
		else if(_val.isUnknown(_literals[_lit1]))//lit2 est faux sinon deductionWL ne serait pas appelé
		{
			_val.modif(abs(_literals[_lit1]), _literals[_lit1] > 0, this);
		}
		return true;
	}

	static ClauseWL resolution(ClauseWL c1, ClauseWL c2, var_t var)
	{
		ClauseWL c = new ClauseWL;

		c._resolution1 = c1;
		c._resolution2 = c2;

		debug (2) {
			writeln("DEBUG: Résolution pour ", var, " :");
			writeln(" ", c1);
			writeln(" ", c2);
		}

		auto r1 = c1._literals;
		auto r2 = c2._literals;
		while (r1.length != 0 || r2.length != 0) {
			debug(2) {
				writeln("ns:");
				writeln(r1);
				writeln(r2);
			}
			if (r1.length != 0 && (r2.length == 0 || alessThan(r1[0], r2[0]))) {
				if (abs(r1[0]) != var) {
					c.append_literal(r1[0]);
				}
				r1 = r1[1 .. $];
			} else {
				if (abs(r2[0]) != var) {
					c.append_literal(r2[0]);
				}
				r2 = r2[1 .. $];
			}
		}
		
		debug (2) {
			writeln("Résultat : ", c);
		}
		return c;
	}
}
