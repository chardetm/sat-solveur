﻿module types.clause;

public import types.basic_types;

import types.valuation, types.arbre;
import std.math, std.exception,std.conv, std.stdio;

version(watchedLiterals) {
	import algorithms.watched_literals;
}

class Clause {
	
	static protected Valuation _val;

	protected lit_t[] _literals;
	protected Clause _resolution1 = null;
	protected Clause _resolution2 = null;

	this()
	{
		_literals.length = 0;
	}

	this(Clause c)
	{
		_literals = c.getLiterals;
		_resolution1 = c._resolution1;
		_resolution2 = c._resolution2;
	}

	static void init (Valuation val) {
		_val = val;
	}
	
	@property lit_t[] getLiterals() const
	{
		return _literals.dup;
	}

	static @property Valuation associatedValuation (){
		return _val;
	}
	
	// Ajout d'un littéral dans l'ordre des valeurs absolues, négatif avant positif
	void add_literal (lit_t l) {
		if (has_literal(l))
			return;
		size_t i = _literals.length;
		++_literals.length;
		for (; i>0 && !alessThan(_literals[i-1], l); --i) {
			_literals[i] = _literals[i-1];
		}
		_literals[i] = l;
	}

	void append_literal (lit_t l) {
		if (_literals.length > 0) {
			if (l == _literals[$-1]) return; // On n'ajoute pas deux fois le même litéral
			if (_literals.length > 0)
				assert (alessThan(_literals[$-1], l));
		}
		_literals~=l;
	}
	
	
	// Recherche dichotomique
	bool has_literal (lit_t l) const {
		bool has_literal_aux (const lit_t[] array, lit_t l) const {
			if (array.length == 0) {
				return false;
			} else {
				auto mid = array[array.length/2];
				if (mid == l) {
					return true;
				} else if (alessThan(mid, l)) {
					return has_literal_aux(array[(array.length/2)+1 .. $],l);
				} else {
					return has_literal_aux(array[0 .. array.length/2],l);
				}
			}
		}

		return has_literal_aux(_literals, l);
	}
	
	
	// Vrai si au moins un littéral est vrai
	@property bool isSatisfied () const {
		foreach (lit ; _literals) {
			if (lit < 0) {
				auto value = _val.getVal(-lit);
				if (value == Faux) {
					return true;
				}
			} else {
				auto value = _val.getVal(lit);
				if (value == Vrai) {
					return true;
				}
			}
		}
		return false;
	}
	
	
	@property bool isTautology () {
		
		for(size_t i=0; i < _literals.length-1; ++i) {
			// Comme les litéraux sont triés par valeur absolue et non dupliqués,
			// si deux litéraux de suite ont la même valeur absolue, la clause est
			// une tautologie et inversement
			if (abs(_literals[i]) == abs(_literals[i+1])) {
				return true;
			}
		}

		return false;
	}
	
	

	@property ulong length() const {
		return _literals.length;
	}

	/*
	 * Une clause est est un monome si elle n'est pas satisfaite,
	 * et si un seul littéral n'est pas défini
	 */
	@property bool isMonome () {
		bool oneUndefined = false;
		foreach (l; _literals) {
			if (_val.isTrue(l)) {
				return false;
			} else if (_val.isUnknown(l)) {
				if (oneUndefined) {
					return false;
				} else {
					oneUndefined = true;
				}
			}
		}
		return oneUndefined;
	}

	@property ulong etageMax() const {
		ulong max = 0;
		foreach(e;_literals)
		{
			ulong eta = _val.getEtage(abs(e));
			max = max < eta ? eta : max;
		}
		return max;
	}


	@property bool isEmpty () const { // If only false variables
		foreach (l; _literals) {
			if (_val.isTrue(l) || _val.isUnknown(l)) {
				return false;
			}
		}
		return true;
	}


	lit_t getLiteral (size_t i) const {
		return _literals[i];
	}


	lit_t getFirstUnknownLiteral () const {
		foreach (l; _literals) {
			if (_val.isUnknown(l)) {
				return l;
			}
		}
		return 0;
	}
	
	
	// Fonction de tri (ordre total)
	protected static bool alessThan (lit_t a, lit_t b) {
		if (abs(a) < abs(b) || (abs(a) == abs(b) && a <= b)) {
			return true;
		} else {
			return false;
		}
	}

	void copy (Clause c) {
		_literals = c._literals.dup;
	}

	Clause activeClause () const {
		Clause nc = new Clause;
		foreach (l; _literals) {
			if (_val.isUnknown(l)) {
				nc.add_literal(l);
			}
		}
		return nc;
	}

	//TODO: Améliorer complexité (algo "fusion de deux listes triées")
	static Clause resolution(Clause c1, Clause c2, var_t var)
	{
		Clause c = new Clause;

		c._resolution1 = c1;
		c._resolution2 = c2;
		
		debug (2) {
			writeln("DEBUG: Résolution pour ", var, " :");
			writeln(" ", c1);
			writeln(" ", c2);
		}
		
		auto r1 = c1._literals;
		auto r2 = c2._literals;
		while (r1.length != 0 || r2.length != 0) {
			debug(2) {
				writeln("ns:");
				writeln(r1);
				writeln(r2);
			}
			if (r1.length != 0 && (r2.length == 0 || alessThan(r1[0], r2[0]))) {
				if (abs(r1[0]) != var) {
					c.append_literal(r1[0]);
				}
				r1 = r1[1 .. $];
			} else {
				if (abs(r2[0]) != var) {
					c.append_literal(r2[0]);
				}
				r2 = r2[1 .. $];
			}
		}
		
		debug (2) {
			writeln("Résultat : ", c);
		}
		return c;
	}

	@property Clause resolution1 ()
	{
		return _resolution1;
	}
	@property Clause resolution2 ()
	{
		return _resolution2;
	}

	@property var_t[] getNew() const
	{
		// Renvoie les variables de la clause qui ont été déduites dans l'étage actuel
		var_t[] res;
		foreach(e; _literals)
		{
			if(_val.getEtage(abs(e)) == _val.etage)
				res ~= abs(e);
		}

		return res;
	}

	static var_t getNonPariFirstUIP(var_t[] t)// Permet d'assurer qu'on arrive au premier UIP
	{
		ulong i = 0;
		int max = _val.getDeducTrack(t[i]);
		foreach(j; 1..t.length)
		{
			if(_val.getDeducTrack(t[j]) > max)
			{
				max = _val.getDeducTrack(t[j]);
				i = j;
			}
		}
		return t[i];
	}

	static var_t getNonPari(var_t[] t)
	{
		if(_val.getDeducTrack(t[0]) == 0)
			return t[1];
		else
			return t[0];
	}

	override string toString () const {
		string str;
		foreach(e; _literals)
		{
			str~= to!string(e);
			str ~= " ";
		}
		str~="0";
		return str;
	}

	bool egal(Clause c) const
	{
		if(c is null || c.length != _literals.length)
			return false;

		foreach (i; 0.._literals.length)
		{
			if (_literals[i] != c._literals[i])
				return false;
		}

		return true;
	}

	override bool opEquals(Object o) const
	{
		if(typeid(this) == typeid(o))
			return egal(cast(Clause) o);
		else
			return false;
	}

	ulong tailleInconnu() const
	{
		ulong rep = 0;
		foreach(e;_literals)
		{
			if(_val.getVal(abs(e)) == Inconnu)
				rep++;
		}
		return rep;
	}
	void augmenteScore()
	{
		foreach(e;_literals)
		{
			if(_val.getVal(abs(e)) == Inconnu)
				_val.augmenteScore(e);
		}
	}

	Arbre toArbre()
	{
		Arbre a;
		if(_literals.length == 0)
			return a;
		if(_literals[0] < 0)
		{
			a = new Arbre("~");
			a.addFils(new Arbre(to!string(-_literals[0])));
		}
		else
		{
			a = new Arbre(to!string(_literals[0]));
		}
		foreach(i;1.._literals.length)
		{
			Arbre te = new Arbre("\\\\/");
			te.addFils(a);
			Arbre lit;
			if(_literals[i] < 0)
			{
				lit = new Arbre("~");
				lit.addFils(new Arbre(to!string(-_literals[i])));
			}
			else
				lit = new Arbre(to!string(_literals[i]));
			te.addFils(lit);
			a = te;
		}
		return a;
	}
}

