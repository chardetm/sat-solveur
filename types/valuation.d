module types.valuation;

import std.stdio, std.conv, std.string, std.exception, std.math, std.random;
public import types.basic_types;
import types.clause, types.grapheCNF;

enum nivMax = 2;


class Valuation
{
	protected valu_t[] _variable; //tableau des valeurs de chaque variable.
	protected int[] _nbPari; //_nbPari[i] = 0 au début, 1 quand on fait un pari sur la variable i et 2 quand on a fait deux pari sur la variable i
	protected var_t[] _pilePari; //on peut l'enlever normalement mais backtrack "normal" est plus simple avec
	protected var_t _lastChVar;
	protected var_t _V;
	protected etage_t _etage;
	protected ulong _reste;
	protected etage_t[] _etageVar; //_etageVar[i] = k si l'etage de la variable i est k.
	protected Clause[] _raisonModif; //_raisonModif[i] = C si i a été déduit de la clause C.
	protected pari_t _modePari;
	protected double[] _score;

	//UIP
	protected int[] _deducTrack;
	protected int _compteurDeduc = 0;

	this()
	{

	}

	this(pari_t choix) 
	{
		_modePari = choix;
	}

	void init(var_t V)
	{
		_V = V;
		_variable.length = V+1;
		_nbPari.length = V+1;
		_etageVar.length = V+1;
		_raisonModif.length = V + 1;
		_deducTrack.length = V+1;
		_score.length = 2*V+1;

		_score[] = 0;
		_nbPari[] = 0;
		_variable[] = Inconnu;
		_etageVar[] = 0;
		_raisonModif[] = new Clause;
		_etage = 0;
		_deducTrack[] = 0;

		_lastChVar = 0;
		_reste = V;
	}

	@property bool fairePari(const ref Clause[] ensembleClause)
	{
		if(_reste == 0)
		{
			// Toutes les variables ont une valeur : l'algorithme s'arrête
			return false;
		}
		debug(2) {
			writeln("Debug: Pari fait (valuation)");
			//should not go there anymore
			if(reste2 == 0)
				writeln("ERROR : reste alors que reste pas :'(", _reste);
		}
		_reste--;
		_etage++;
		lit_t futurLit = nextPari(ensembleClause);
		var_t futurPari = abs(futurLit);
		_variable[futurPari] = futurLit > 0 ? Vrai : Faux;
		_etageVar[futurPari] = _etage;
		// futurPari est un pari (no kidding) -> dans la pile pari !
		_pilePari ~= futurPari;
		_lastChVar = futurPari;
		_nbPari[futurPari] = 1;
		_deducTrack[futurPari] = 0;
		_compteurDeduc = 1;
		foreach(i;1.._score.length)
		{
			_score[i] /=2;//pour privilegier la nouveauté
		}
		// on a fait un pari, on continue l'algo.
		return true;
	}

	@property bool smartFairePari(var_t v, bool val = true)//pour pari intelligent
	{
		if(_variable[v] != Inconnu)
		{
			debug
			{
				writeln("Pari sur variable déjà attribuée. Probablement une erreur.");
			}
			return false;
		}
		else
		{
			_reste--;
			_etage++;
			_pilePari ~= v;
			_lastChVar = v;
			_deducTrack[v] = 0;
			_compteurDeduc = 1;
			_nbPari[v] = 1;
			_variable[v] = val ? Vrai : Faux;
			_etageVar[v] = _etage;
			return true;
		}
	}

	/**
	 *  Affecte Vrai ou Faux (val) à la variable var, en indiquant la clause c responsable pour cette affectation.
	 *  Renvoie vrai ssi il n'y a pas eu de problème (variable déjà affectée de manière opposée)
	 */
	bool modif (var_t var, bool val, Clause c)
	{
		// Renvoie vrai si la modif a réussi
		// renvoie faux sinon
		enforce(var < _variable.length, "Error: variable off limits!");
		debug
		{
			if (_variable[var] != Inconnu)
				writeln("Warning: attribution d'une valeur déjà définie");
		}
		// Si on a un monome, on veut que "l'étage" de la variable soit 0
		// en effet, on ne veut pas qu'on puisse annuler cette valeur (étant donné que le monome sera toujours là)
		if(c !is null && c.length == 1)
		{
			if((_variable[var] == Vrai && !val) || (_variable[var] == Faux && val))
			{
				return false;
			}
			_reste--;
			_variable[var] = val ? Vrai : Faux;
			_lastChVar = var;
			_etageVar[var] = 0;
			_raisonModif[var] = c;
			return true;
		}
		_reste--;
		_variable[var] = val ? Vrai : Faux;
		_lastChVar = var;
		_etageVar[var] = _etage;
		_raisonModif[var] = c;
		_deducTrack[var] = _compteurDeduc;
		_compteurDeduc++;
		return true;
	}
	int getDeducTrack(var_t var) const
	{
		return _deducTrack[var];
	}

	valu_t getVal(var_t var)
	{
		enforce(var < _variable.length, "Error: variable off limits!");
		return _variable[var];
	}
 
	Clause getRaisonModif(var_t var)
	{
		enforce(var < _variable.length, "Error: variable off limits!");
		return _raisonModif[var];
	}

	ulong getEtage(var_t i) const
	{
		enforce(i < _variable.length, "Error: variable off limits!");
		return _etageVar[i];
	}

	@property ulong etage() const
	{
		return _etage;
	}

	bool isTrue(lit_t lit) const
	{
		if(lit > 0)
		{
			return _variable[lit] == Vrai;
		}
		else
		{
			return _variable[-lit] == Faux;
		}
	}
	
	bool isFalse(lit_t lit) const
	{
		if(lit > 0)
		{
			return _variable[lit] == Faux;
		}
		else
		{
			return _variable[-lit] == Vrai;
		}
	}
	
	bool isUnknown(lit_t lit) const
	{
		return _variable[abs(lit)] == Inconnu;
	}

	@property ulong length() const
	{
		return _variable.length;
	}

	void backtrack()
	{
		if(_pilePari.length == 0)
		{
			throw new NotSolvable("Backtrack sur pile pari vide");
		}
		var_t dernierPari = _pilePari[$-1];
		foreach(var_t i; 1.._V+1)
		{
			if(_etageVar[i] == _etage && i != dernierPari) // on efface tout ceux de l'étage actuel
			{
				_etageVar[i] = 0;
				_variable[i] = Inconnu;
				_raisonModif[i] = new Clause;
				_reste++;
			}
		}
		if(_nbPari[dernierPari] == 1)
		{
			//on a testé qu'une seule valeur de dernierPari
			//maintenant on teste la deuxième
			_nbPari[dernierPari] = 2;
			_variable[dernierPari] = (_variable[dernierPari] == Vrai ? Faux : Vrai);
			_lastChVar = dernierPari;
		}
		else
		{
			//on a testé les deux valeurs de dernierPari. On remet donc sa valeur à Inconnu...
			_nbPari[dernierPari] = 0;
			_variable[dernierPari] = Inconnu;
			_pilePari.length--;
			_etage--;
			_reste++;
			_etageVar[dernierPari] = 0;
			//... et on continue le backtracking.
			backtrack();
		}
	}

	long backtrack(var_t var, Clause newC)
	{
		//nouveau backtrack
		if(_etage == 0)
		{
			//_etage 0 -> aucun pari n'a été fait
			//on ne peut donc pas remonter
			return -1;
		}
		_etageVar[var] = 0;
		//on met l'étage de var à 0 pour que newC.etageMax nous renvoie l'étage maximal sans var!
		//De toute façon la valeur de var doit aller à inconnu
		_variable[var] = Inconnu;
		_raisonModif[var] = new Clause;
		_reste++;
		ulong nouvEtage = newC.etageMax;
		_etage = nouvEtage;

		foreach(vari;1.._V+1)
		{
			if(_etageVar[vari] > nouvEtage)
			{
				//on efface tout ceux qui ont été déduit après le nouvel étage.
				_variable[vari] = Inconnu;
				_raisonModif[vari] = new Clause;
				_etageVar[vari] = 0;
				_reste++;
			}
		}
		return nouvEtage; // Pour des raisons de debugage, on renvoie le nouvel étage
	}

	void copy(Valuation val)
	{
		_variable = val._variable.dup;
		_pilePari = val._pilePari.dup;
		_lastChVar = val._lastChVar;
		_nbPari = val._nbPari;
		_etage = val._etage;
		_etageVar = val._etageVar.dup;
	}

	@property ulong reste() const // renvoie le nombre de variables sans valeur.
	{
		return _reste;
	}

	deprecated @property ulong reste2() const
	{
		//normalement useless
		ulong c = 0;
		foreach(i; 1.._V+1)
		{
			if(_variable[i] == Inconnu)
				c++;
		}
		return c;
	}

	@property var_t nbVariables() const { return _V; }

	@property var_t dernierPari() const
	{
		return _pilePari[$-1];
	}
	
	@property var_t dernierChVar() const
	{
		return _lastChVar;
	}

	@property override string toString() const
	{
		string str = "";
		for(int i = 1; i < _variable.length; i++)
		{
			str ~= to!string(i*(_variable[i] == Vrai ? 1 : -1)*(_variable[i] == Inconnu ? 0 : 1)) ~ " ";
		}
		str ~= "0";
		return str;
	}

	void creerContradProof(Clause c, var_t v)
	{
		while(c.length > 0)
		{
			// creation de la clause vide
			c = Clause.resolution(c, _raisonModif[v], v);
			if(c.length > 0)
				v = abs(c.getLiterals[0]);
		}

		Clause[] subSet;
		subSet.length = 0;

		File f;
		// arbre de dérivation de la clause vide
		f.open("contrad.tex", "w");
		f.writeln("\\documentclass[french]{report}");

		f.writeln("\\usepackage[utf8]{inputenc}");
		f.writeln("\\usepackage[T1]{fontenc}");
		f.writeln("\\usepackage[francais]{babel}");
		f.writeln("\\usepackage[a4paper,landscape]{geometry}");
		f.writeln("\\usepackage{bussproofs}");
		f.writeln;
		f.writeln("\\begin{document}");
		string[] script = faireFichierTex(c, subSet);
		f.writeln(script[0]);
		f.writeln("\\begin{prooftree}");
		f.writeln(script[1]);
		f.writeln("\\end{prooftree}");
		f.writeln("\\end{document}");

		f.open("contrad.cnf", "w");
		//fichier cnf du problème reduit à un sous ensemble suffisant à faire unsat
		f.writeln("p cnf ", _variable.length-1, " ", subSet.length);
		foreach(e;subSet)
		{
			f.writeln(e);
		}
	}

	static void ajouter(ref Clause[] t, Clause c)
	{
		if(c.resolution1 is null)
		{
			if(!isIn(t, c))
			{
				t ~= c;
			}
			return;
		}
		ajouter(t, c.resolution1);
		ajouter(t, c.resolution2);
	}
	static bool isIn(Clause[] t, Clause c)
	{	
		if(t.length == 0)
		{
			return false;
		}
		if(c == t[0])
			return true;
		return isIn(t[1..$], c);
	}
	string[] faireFichierTex(Clause c, ref Clause[] t, int niv = 0)
	{
		//rep[0] : arbres déjà fait (pour éviter d'avoir un arbre de taille 42 qui tient pas sur l'écran)
		//rep[1] : arbre en cours de création
		string[] rep;
		rep.length = 2;
		string lines = "";
		if(c.resolution1 is null)
		{
			if(!isIn(t, c))
				t ~= c;
			lines~="\\AxiomC{}\n";
			lines~="\\UnaryInfC{" ~ c.toString ~ "}\n";
			rep[0] = "";
			rep[1] = lines;
		}
		else
		{
			string[] res1;
			string[] res2;
			if(niv == nivMax)
			{
				res1 = faireFichierTex(c.resolution1, t, 0);
				res2 = faireFichierTex(c.resolution2,t,0);
				lines ~= res1[0];
				lines ~= res2[0];
				lines ~= "\\begin{prooftree}\n";
				lines ~= res1[1];
				lines ~= res2[1];
				lines ~="\\BinaryInfC{" ~ c.toString ~ "}\n";
				lines ~= "\\end{prooftree}\n";
				rep[0] = lines;
				lines = "";
				lines~= "\\AxiomC{" ~ c.toString ~ "}\n";
				rep[1] = lines;
			}
			else
			{
				res1 = faireFichierTex(c.resolution1,t,niv+1);
				res2 = faireFichierTex(c.resolution2,t, niv+1);
				lines~=res1[0];
				lines~=res2[0];
				rep[0] = lines;
				lines = "";
				lines ~= res1[1];
				lines ~= res2[1];
				lines~="\\BinaryInfC{" ~ c.toString ~ "}\n";
				rep[1] = lines;
			}
		}

		return rep;
	}

	var_t V() const
	{
		return _V;
	}

	lit_t nextPari(const ref Clause[] ensembleClause)
	{
		lit_t futurPari;
		if(_modePari == pari_t.NORMAL)
		{
			futurPari = 1;
			while(_variable[futurPari] != Inconnu)
			{
				futurPari++;
			}
		}
		else if(_modePari == pari_t.RAND)
		{
			var_t[] varInconnu;
			foreach(i; 1..V+1)
			{
				if(_variable[i] == Inconnu)
					varInconnu ~= i;
			}
			futurPari = varInconnu[uniform(0,varInconnu.length)];
		}
		else if(_modePari == pari_t.VSIDS)
		{
			futurPari = 1;
			float score;
			if(_variable[futurPari] != Inconnu)
				score = 0;
			else
				score = _score[futurPari];
			foreach(i;2..V+1)
			{
				if(_score[i] > score && _variable[i] == Inconnu)
				{
					futurPari = i;
					score = _score[i];
				}
			}
			foreach(i;V+1..2*V+1)
			{
				if(_score[i] > score && _variable[i-V] == Inconnu)
				{
					futurPari = i;
					score = _score[i];
				}
			}
			if(score == 0)
			{	
				futurPari = 1;
				while(_variable[futurPari] != Inconnu)
				{
					futurPari++;
				}				
			}
			else if(futurPari >= V+1)
			{
				futurPari = -(futurPari-V);
			}
			foreach(i;1..2*V+1)
			{
				_score[i] /= 2;
			}

		}
		else if(_modePari == pari_t.MOMS)
		{
			ulong tailleMini = ulong.max;
			foreach(e; ensembleClause)
			{
				ulong tI = e.tailleInconnu;
				if(!e.isSatisfied && tI < tailleMini)
					tailleMini = tI;
			}
			ulong[] nbApparition;
			nbApparition.length = 2*V+1;
			foreach(e; ensembleClause)
			{
				if(e.tailleInconnu == tailleMini && !e.isSatisfied)
				{
					foreach(v; e.getLiterals)
					{
						if(_variable[abs(v)] == Inconnu)
						{
							if(v > 0)
								nbApparition[v]++;
							else
								nbApparition[-v+V]++;
						}
					}
				}
			}
			futurPari = 1;
			foreach(i;2..2*V+1)
			{
				if(nbApparition[i] > nbApparition[futurPari])
					futurPari = i;
			}
			if(nbApparition[futurPari] == 0)
			{	
				futurPari = 1;
				while(_variable[futurPari] != Inconnu)
				{
					futurPari++;
				}
			}
			else if(futurPari >= V+1)
			{
				futurPari = -(futurPari-V);
			}
		}
		else if(_modePari == pari_t.DLIS)
		{
			ulong[] nbClauseSatisfaite;
			nbClauseSatisfaite.length = 2*V+1;
			nbClauseSatisfaite[] = 0;

			foreach(e;ensembleClause)
			{
				if(!e.isSatisfied)
				{
					foreach(i;e.getLiterals)
					{
						if(_variable[abs(i)]==Inconnu)
						{
							if(i > 0)
								nbClauseSatisfaite[i]++;
							else
								nbClauseSatisfaite[V-i]++;
						}
					}
				}
			}

			futurPari = 1;
			foreach(i;2..2*V+1)
			{
				if(nbClauseSatisfaite[i] > nbClauseSatisfaite[futurPari])
					futurPari = i;
			}
			if(nbClauseSatisfaite[futurPari] == 0)
			{	
				futurPari = 1;
				while(_variable[futurPari] != Inconnu)
				{
					futurPari++;
				}
			}
			else if(futurPari >= V+1)
			{
				futurPari = -(futurPari-V);
			}
		}
		return futurPari;
	}
	void augmenteScore(lit_t l)
	{
		if(l < 0)
		{
			_score[-l+V]++;
		}
		else
			_score[l]++;
	}
	valu_t[] variable()
	{
		return _variable.dup;
	}
}
