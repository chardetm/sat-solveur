module types.graphe;

/*
*
*
*	GRAPHE NORMAL : PAS UTILISABLE POUR FAIRE UN ARBRE DE CONFLIT !
*
*
*/

import types.basic_types, types.valuation, types.clause, types.instance;
import std.stdio, std.math, std.conv, std.string, std.exception , std.process;

class Graphe
{
	protected bool[][] _matriceAdj;
	protected ulong _S; //(sommet allant de 1 à S);
	protected ulong[] _couleur; //couleur d'un noeud
	protected string[] _palette; //ensemble de couleurs disponible

	this()
	{
		_S = 0;
		_couleur.length = 1;
		_matriceAdj.length = 1;
		_matriceAdj[0].length = 1;
		_matriceAdj[0][0] = false;
		// couleurs de base
		// possibilité de faire un générateur aléatoire de couleur SI VRAIMENT C'EST NECESSAIRE
		_palette ~= "white";
		_palette ~= "red";
		_palette ~= "yellow";
		_palette ~= "blue";
		_palette ~= "magenta";
		_palette ~= "cyan";
		_palette ~= "green";
		_palette ~= "brown";
		_palette ~= "orange";
		_palette ~= "black";
		_palette ~= "gray";
	}

	bool addEdge(ver_t a1, ver_t a2)
	{
		ulong maxi = a1 < a2 ? a2 : a1;
		if(maxi > _S)
		{
			// Il faut augmenter la taille de _matriceAdj et initialiser les nouvelles valeurs à false
			// On note que _matriceAdj[0][i] et _matriceAjd[i][0] ne sont JAMAIS regardées.
			_matriceAdj.length = maxi+1;
			foreach(i; 1.._S+1)
			{
				_matriceAdj[i].length = maxi+1;
				foreach(j; _S+1..maxi+1)
					_matriceAdj[i][j] = false;
			}
			foreach(i; _S+1..maxi+1)
			{
				_matriceAdj[i].length = maxi+1;
				foreach(j; 1..maxi+1)
				{
					_matriceAdj[i][j] = false;
				}
			}
			_S = maxi;
			_couleur.length = _S+1;
		}
		bool dejaPre = _matriceAdj[a1][a2];
		_matriceAdj[a1][a2] = true;
		_matriceAdj[a2][a1] = true; //graphe non orienté _matriceAdj est symétrique
		return dejaPre;
	}

	void coloriage(ver_t v, ulong c)
	{
		debug {
			if(_couleur[v] != 0)
				writeln("Debug: Attention, on recolorie un sommet. Pas normal. Sérieux.");
		}
		_couleur[v] = c;
	}

	void toDot(string fileName = "graph.dot")
	{
		File f = File(fileName, "w");
		f.writeln("digraph G {");
		f.writeln("size=\"4,4\"");
		foreach(i;1.._S+1)
		{
			f.writeln(i, "[fillcolor=", _palette[_couleur[i]], ",style=filled];");
			foreach(j;i+1.._S+1)
			{
				if(_matriceAdj[i][j])
					f.writeln(i, " -> ", j, " [dir=none];");
			}
		}
		f.writeln("}");
	}

	Instance toInstance(ulong k, pari_t pari, mode_t mode) // k est le nombre de couleurs maximal
	{
		// soit i un entier compris entre 1 et k (inclus)
		// on a que la variable i + (j-1)*k correspond à "sommet j colorié par la couleur i".
		Instance res = new Instance(mode, pari);
		uint C = 0; // nombre de clauses
		Clause newClause = new Clause;
		foreach(j; 1.._S+1)
		{
			// le sommet i doit être colorié
			foreach(i; 1..k+1)
			{
				newClause.add_literal(i+(j-1)*k);
			}
			res.addClause(newClause);
			newClause = new Clause;
			C++;
		}
		foreach(j; 1.._S+1)
		{
			// le sommet i n'a qu'une couleur
			foreach(i1; 1..k+1)
			{
				foreach(i2; 1..k+1)
				{
					if(i1 != i2)
					{
						newClause.add_literal(-(i1+(j-1)*k));
						newClause.add_literal(-(i2+(j-1)*k));
						res.addClause(newClause);
						newClause = new Clause;
						C++;
					}
				}
			}
		}
		foreach(j1; 1.._S+1)
		{
			foreach(j2; j1+1.._S+1)
			{

				if(_matriceAdj[j1][j2])
				{
					// si j1 et j2 sont adjacents, ils sont d'une couleur différente
					foreach(i;1..k+1)
					{
						newClause.add_literal(-(i+(j1-1)*k));
						newClause.add_literal(-(i+(j2-1)*k));
						res.addClause(newClause);
						newClause = new Clause;
						C++;
					}
				}
			}
		}
		//nombre de clauses : pour les _S sommets, il y a k variables (une par couleur) d'où k*_S
		res.init(k*_S, C);
		Clause.init(res.val);//on n'oublie pas de donner la bonne valuation à nos clauses
		return res;
	}
	void coloriageGraph(Valuation val, ulong k)
	{
		foreach(j;1.._S+1)
		{
			foreach(i;1..k+1)
			{
				if(val.getVal(i+(j-1)*k) == Vrai)
				{
					coloriage(j,i);
				}
			}
		}
	}
	void toPDF(string fileName = "graph")
	{
		toDot(fileName ~ ".dot");
		executeShell("dot -Tpdf " ~ fileName ~ ".dot -o " ~ fileName ~ ".pdf");
		executeShell("rm " ~ fileName ~".dot");
	}
}
