module types.entier;

import std.conv, std.stdio;

struct Entier
{
protected:
	long _n;
	bool _inf;

public:
	this(long n = 0, bool inf = false)
	{
		_n = n;
		_inf = inf;
	}

	this(Entier x)
	{
		_n = x._n;
		_inf = x._inf;
	}

	long val() const
	{
		return _n;
	}
	void val(long n)
	{
		_n = n;
	}
	bool inf() const
	{
		return _inf;
	}

	bool egal(Entier n)
	{
		return (_inf && n.inf && _n * n.val > 0) || (!_inf && !n.inf && _n == n.val);
	}

	bool opEquals(Entier o)
	{
		return egal(o);
	}

	Entier opUnary(string s)()
	{
		if(s=="-")
		{
			return Entier(-_n, _inf);
		}
		if(s=="--")
		{
			if(_inf)
				return Entier(this);
			else
				return Entier(_n-1, false);
		}
		if(s=="++")
		{
			if(_inf)
				return Entier(this);
			else
				return Entier(_n+1, false);
		}
	}
	long opCmp(ref const Entier n) const
	{
		if(_inf)
		{
			return _n;
		}
		else if(n._inf)
		{
			return -n.val;
		}
		else
		{
			return (_n - n.val);
		}
	}

	long opCmp(long n) const
	{
		if(_inf)
		{
			return _n;
		}
		else
		{
			return (_n);
		}
	}

	void opOpAssign(string op)(Entier b)
	{
		if(!_inf)
		{
			if(op=="+")
			{
				if(b._inf)
				{
					_inf = true;
					_n = b.val;
				}
				else
				{
					_n += b.val;
				}
			}
			else if(op=="-")
			{
				if(b._inf)
				{
					_inf = true;
					_n = -b.val;
				}
				else
				{
					_n -= b.val;
				}
			}
		}
		if(op=="*")
		{
			_inf = _inf || b._inf;
			_n = _n * b.val;
		}
	}
	Entier opBinary(string op)(Entier b)
	{
		Entier a = Entier(this);
		if(op == "+")
		{
			a += b;
		}
		else if(op == "-")
		{
			a -= b;
		}
		else if(op == "*")
		{
			a *= b;
		}
		return a;
	}
	void opAssign(long n)
	{
		_inf = false;
		_n = n;
	}
	void opAssign(Entier a)
	{
		_inf = a._inf;
		_n = a.val;
	}
	string toString()
	{
		if(_inf)
		{
			if(_n > 0)
			{
				return("+inf");
			}
			else if(_n < 0)
			{
				return ("-inf");
			}
			else
				return "NaN";
		}
		else
			return to!string(_n);
	}
}
