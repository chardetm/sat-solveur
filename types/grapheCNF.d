module types.grapheCNF;

import types.basic_types, types.valuation, types.clause;
import std.stdio, std.math, std.conv, std.string, std.exception, std.process;

class GrapheCNF
{
	protected bool[][] _matriceAdj;
	protected ulong _S; // (sommet allant de 1 à S) [0 est utilisé pour le conflit]
	                    // on manipule des sommets allant de 1 à S
	                    // il faut donc pouvoir faire une lien entre sommet et variable
	                    // c'est l'intérêt des deux tableaux suivants
	protected ulong[] _varToVertex;
	protected var_t[] _vertexToVar;
	protected Valuation _val;

	this(ulong V)
	{
		_S = 0;
		_vertexToVar.length = 1; // case [0] inutilisé [conflit n'est pas une variable!]
		_varToVertex.length = V+1;
		_matriceAdj.length = 1;
		_matriceAdj[0].length = 1;
		_matriceAdj[0][0] = false;
	}

	void setValu(Valuation val)
	{
		_val = val;
	}

	void addEdge(var_t a, var_t b) // rajoute a->b
	{
		_matriceAdj[_varToVertex[a]][_varToVertex[b]] = true;
	}
	void removeEdge(var_t a, var_t b)
	{
		_matriceAdj[_varToVertex[a]][_varToVertex[b]] = false;	
	}
	bool addVertex(var_t var)
	{
		if(_varToVertex[var] !=0)
		{
			//deja dans sommet
			return false;
		}
		_S++;
		_varToVertex[var] = _S;
		_matriceAdj.length++;
		_matriceAdj[_S].length = _S+1;
		for(int i = 0; i <= _S; i++)
		{
			_matriceAdj[_S][i] = false;
		}
		for(int i = 0; i < _S; i++)
		{
			_matriceAdj[i].length++;
			_matriceAdj[i][_S] = false;
		}
		_vertexToVar ~= var;
		return true;
	}
	void create(Clause c)
	{
		// on crée l'arbre de conflit qui est créé par la clause C (la clause C est la clause qui permet de détecter le conflit)
		// on part du conflit, et on remonte au pari
		foreach(e;c.getLiterals)
		{
			addVertex(abs(e));
			_matriceAdj[_varToVertex[abs(e)]][0] = true;
		}
		foreach(e; c.getNew())
		{
			// on continue pour les variables de l'étage actuel
			processClause(_val.getRaisonModif(e),e);
		}
	}
	void processClause(Clause c, var_t var)
	{
		foreach(e; c.getLiterals)
		{
			if(abs(e) == var)//var a déjà été traité
				continue;
			addVertex(abs(e));
			_matriceAdj[_varToVertex[abs(e)]][_varToVertex[var]] = true;
		}
		foreach(e; c.getNew())
		{
			if(e != var) // var a déjà été traité
				processClause(_val.getRaisonModif(e),e); // on continue pour les variables de l'étage actuel
		}
	}

	bool isEdge(var_t a, var_t b)
	{
		return _matriceAdj[_varToVertex[a]][_varToVertex[b]];
	}

	// série de fonctions plus ou moins utiles pour se déplacer dans le graphe
	var_t[] next(var_t a)
	{
		ulong ind = _varToVertex[a];
		var_t[] res;
		res.length = 0;
		for(int i=  1; i <= _S; i++)
		{
			if(_matriceAdj[ind][i])
				res ~= _vertexToVar[i];
		}

		return res;
	}
	var_t[] pred(var_t a)
	{
		ulong ind = _varToVertex[a];
		var_t[] res;
		res.length = 0;
		for(int i=  1; i <= _S; i++)
		{
			if(_matriceAdj[i][ind])
				res ~= _vertexToVar[i];
		}

		return res;
	}

	ulong[] nextIndNew(ulong ind)
	{
		ulong[] res;
		res.length = 0;
		foreach(i;0.._S+1)
		{
			if(_matriceAdj[ind][i] && _val.getEtage(abs(_vertexToVar[i])) == _val.etage)
				res ~= i;
		}
		return res;
	}
	ulong[] nextInd(ulong ind)
	{
		ulong[] res;
		res.length = 0;
		foreach(i;0.._S+1)
		{
			if(_matriceAdj[ind][i])
				res ~= i;
		}
		return res;
	}
	ulong[] predIndNew(ulong ind)
	{
		ulong[] res;
		res.length = 0;
		foreach(i;0.._S+1)
		{
			if(_matriceAdj[i][ind] && _val.getEtage(abs(_vertexToVar[i])) == _val.etage)
				res ~= i;
		}
		return res;
	}
	ulong[] predInd(ulong ind)
	{
		ulong[] res;
		res.length = 0;
		foreach(i;0.._S+1)
		{
			if(_matriceAdj[i][ind])
				res ~= i;
		}
		return res;
	}

	@property var_t[] findFirst()
	{
		// renvoie les noeuds sans "père" (ie, aucun autre noeud n'a une arrête qui va vers ses noeuds)
		bool first = false;
		var_t[] init;
		init.length = 0;
		foreach(ulong ind;0.._S+1)
		{
			first = true;
			foreach(ulong i;0.._S+1)
			{
				first = !_matriceAdj[i][ind];
				if(!first)
					break;
			}
			if(first)
			{
				init ~= _vertexToVar[ind];
			}
		}
		return init;
	}

	void toDot(string str = "graph.dot")
	{
		// Dessine TOUT l'arbre de conflit
		File f = File(str, "w");
		f.writeln("digraph G {");
		f.writeln("size =\"4,4\"");
		var_t[] pile;
		var_t[] dejaFait;
		dejaFait.length = 0;
		pile.length = 0;
		pile ~= findFirst;
		dejaFait ~= pile[0];
		while(pile.length > 0)
		{
			long tete = cast(long)pile[0];
			pile = pile[1..$];
			var_t[] suite = next(tete);
			bool neg = _val.getVal(tete) == Faux;
			if(neg)
				f.write("-");
			f.write(tete);
			if(_val.getEtage(tete) == _val.etage)
			{
				f.write(" [color=blue,style=filled]");
			}
			f.writeln(";");
			foreach(e;suite)
			{
				bool negS = (_val.getVal(e)==Faux);
				if(neg)
					f.write("-");
				f.write(tete, " -> ");
				if(negS)
					f.write("-");
				f.writeln(e, ";");
			}
			fusion(pile, suite, dejaFait);
			fusion(dejaFait, suite, dejaFait);
		}
		f.writeln("conflict [color = red, style=filled];");
		foreach(ulong i; 1.._S+1)
		{
			if(_matriceAdj[i][0])
			{
				bool neg = _val.getVal(_vertexToVar[i]) == Faux;
				if(neg)
					f.write("-");
				f.writeln(_vertexToVar[i], " -> conflict;");
			}
		}
		f.writeln("}");
	}

	void toDot(Clause c, var_t pivot, string fileName = "graph.dot")
	{
		// Dessine seulement l'arbre de conflit utile pour déterminer la clause c
		// La clause c est la clause qui va être rajoutée au problème
		File f = File(fileName, "w");
		f.writeln("digraph G {");
		f.writeln("size =\"4,4\"");
		var_t[] pile;
		var_t[] dejaFait;
		dejaFait.length = 0;
		pile.length = 0;
		var_t[] predes = pred(pivot);
		dejaFait ~= predes;
		foreach(e;c.getLiterals)
		{
			// On commence par les noeuds de la clause
			dejaFait ~= abs(e);
		}
		foreach(e;predes)
		{
			// On rajoute les noeuds qui sont dans la clause qui permet de déduit l'UIP
			if(_val.getVal(e) == Faux)
				f.write("-");
			f.write(e, " ");
			if(_val.getEtage(e) == _val.etage)
				f.write("[color=blue, style=filled]");
			f.writeln(";");

			if(_val.getVal(e) == Faux)
				f.write("-");
			f.write(e, " -> ");
			if(_val.getVal(pivot) == Faux)
				f.write("-");
			f.writeln(pivot, " ;");
		}
		foreach(e;c.getLiterals)
		{
			// Puis on va vers le conflit
			var_t[] suite = next(abs(e));
			bool neg = _val.getVal(abs(e)) == Faux;
			if(neg)
				f.write("-");
			f.write(abs(e), " ");
			if(abs(e) == pivot)
				f.writeln("[color=yellow,style=filled];");
			else
				f.writeln("[color=purple, style=filled];");

			foreach(el;suite)
			{
				if(el != pivot)// Déjà traîté dans predes
				{
					bool negS = _val.getVal(el) == Faux;
					if(neg)
						f.write("-");
					f.write(abs(e), " -> ");
					if(negS)
						f.write("-");
					f.writeln(el, " ;");
				}
			}
			fusion(pile, suite, dejaFait);
			fusion(dejaFait, suite, dejaFait);
		}
		while(pile.length > 0)
		{
			// Boucle qui permet d'aller vers le conflit
			var_t tete = pile[0];
			pile = pile[1..$];
			var_t[] suite = next(tete);
			bool neg = _val.getVal(tete) == Faux;
			if(neg)
				f.write("-");
			f.write(tete);
			if(_val.getEtage(tete) == _val.etage)
			{
				f.write(" [color=blue,style=filled]");
			}
			f.writeln(";");
			foreach(e;suite)
			{
				bool negS = (_val.getVal(e)==Faux);
				if(neg)
					f.write("-");
				f.write(tete, " -> ");
				if(negS)
					f.write("-");
				f.writeln(e, ";");
			}
			fusion(pile, suite, dejaFait);
			fusion(dejaFait, suite, dejaFait);
		}
		f.writeln("conflict [color = red, style=filled];");
		foreach(ulong i; 1.._S+1)
		{
			if(_matriceAdj[i][0])
			{
				bool neg = _val.getVal(_vertexToVar[i]) == Faux;
				if(neg)
					f.write("-");
				f.writeln(_vertexToVar[i], " -> conflict;");
			}
		}
		f.writeln("}");	
	}

	void toPDF(Clause c, var_t v, string fileName = "graph.dot")
	{
		toDot(c, v, fileName);
		Pid pid = spawnShell("dot -Tpdf " ~ fileName ~ " -o " ~ fileName[0..$-3] ~ "pdf");
		wait(pid);
		spawnShell("rm " ~ fileName);
	}

	static bool isIn(ulong[] t1, ulong e)
	{
		bool res = false;
		foreach(el; t1)
		{
			if(el == e)
			{
				res = true;
				break;
			}
		}

		return res;
	}
	static void fusion(ref ulong[] t1, ulong[] t2, ulong[] t3)
	{
		// renvoie t1 union [t2 privé de t3]
		foreach(e;t2)
		{
			if(!isIn(t3,e))
			{
				t1 ~= e;
			}
		}
	}

	var_t findFirstNew()
	{
		// renvoie le pari (seul noeud de l'étage actuel sans père)
		var_t[] first = findFirst();
		ulong res = 0;
		while(_val.getEtage(first[res]) != _val.etage)
		{
			res++;
		}
		return first[res];
	}

	var_t toVariable(ulong ind)
	{
		return _vertexToVar[ind];
	}

	ulong[] intersection(ulong[] t1, ulong[] t2)
	{
		ulong[] res;
		res.length = 0;
		foreach(e;t1)
		{
			if(isIn(t2, e))
			{
				res ~= e;
			}
		}

		return res;
	}
}
