﻿module compiler_fixes.string;

inout(char)[] fromStringz(inout(char)* cString) @system pure {
	import core.stdc.string : strlen;
	return cString ? cString[0 .. strlen(cString)] : null;
}