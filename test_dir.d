﻿module test_dir;

// ssh mchardet@servsls.ens-lyon.fr


import std.stdio, std.conv, std.string, std.process, std.exception, std.file, std.path;


int main (string[] args) {
	if (args.length != 2) {
		writeln("Wrong number of arguments, usage: ", args[0], " dir");
		return 1;
	}
	
	string[] errors;
	
	string[] command = [
		"TIMEFORMAT='%3U'; time ./resol ",
		"TIMEFORMAT='%3U'; time ./resol -cl ",
		"TIMEFORMAT='%3U'; time ./resol -rand ",
		"TIMEFORMAT='%3U'; time ./resol -cl -rand ",
		"TIMEFORMAT='%3U'; time ./resol -moms ",
		"TIMEFORMAT='%3U'; time ./resol -cl -moms ",
		"TIMEFORMAT='%3U'; time ./resol -dlis ",
		"TIMEFORMAT='%3U'; time ./resol -cl -vsids ",
		"TIMEFORMAT='%3U'; time ./resol -dlis ",
		"TIMEFORMAT='%3U'; time ./resol -cl -vsids ",
		"TIMEFORMAT='%3U'; time ./resol-wl ",
		"TIMEFORMAT='%3U'; time ./resol-wl -cl ",
		"TIMEFORMAT='%3U'; time ./resol-wl -rand ",
		"TIMEFORMAT='%3U'; time ./resol-wl -cl -rand ",
		"TIMEFORMAT='%3U'; time ./resol-wl -moms ",
		"TIMEFORMAT='%3U'; time ./resol-wl -cl -moms ",
		"TIMEFORMAT='%3U'; time ./resol-wl -dlis ",
		"TIMEFORMAT='%3U'; time ./resol-wl -cl -vsids ",
		"TIMEFORMAT='%3U'; time ./resol-wl -dlis ",
		"TIMEFORMAT='%3U'; time ./resol-wl -cl -vsids ",
		"TIMEFORMAT='%3U'; time minisat "
	];

	string[] caption = [
		"DPLL",
		"DPLL-CL",
		"DPLL_RAND",
		"DPLL-CL_RAND",
		"DPLL_MOMS",
		"DPLL-CL_MOMS",
		"DPLL_DLIS",
		"DPLL-CL_DLIS",
		"DPLL_VSIDS",
		"DPLL-CL_VSIDS",
		"WL",
		"WL-CL",
		"WL_RAND",
		"WL-CL_RAND",
		"WL_MOMS",
		"WL-CL_MOMS",
		"WL_DLIS",
		"WL-CL_DLIS",
		"WL_VSIDS",
		"WL-CL_VSIDS",
		"Minisat"
	];
	
	size_t name_space = 0; // Minimum
	size_t data_space = 0; // Minimum

	if (data_space < 7) data_space = 7;   // Temps   + deux espaces
	if (name_space < 10) name_space = 10; // Fichier + trois espaces
	foreach (c; caption) {
		if (c.length + 2 > data_space) data_space = c.length + 2;
	}
	
	try {
		foreach (string f; dirEntries(args[1], "*.cnf", SpanMode.shallow)) {
			if (baseName(f).length+3 > name_space) name_space = baseName(f).length+3;
		}
		std.stdio.write(leftJustify("Fichier", name_space, ' '));
		foreach (s; caption) {
			std.stdio.write(leftJustify(s, data_space, ' '));
		}
		std.stdio.writeln();
		
		foreach (string f; dirEntries(args[1], "*.cnf", SpanMode.shallow)) {
			bool skip = false, first = true;
			foreach (c; command) {
				auto ret = executeShell(c~f~" >/dev/null 2>/dev/null");
				if (ret.status != 0 && ret.status != 10 && ret.status != 20) {
					errors~=f;
					skip = true;
					break;
				}
				if (first) {
					first = false;
					write(leftJustify(baseName(f), name_space, ' '));
				}
				string result = translate(chomp(ret.output), ['.' : ',']);
				std.stdio.write(leftJustify(result, data_space, ' '));
				stdout.flush();
			}
			if (!skip)
				writeln();
		}
		stderr.writeln();
		stderr.writeln(errors.length, " errors: ");
		foreach (e ; errors) {
			stderr.writeln("  ", e);
		}
	} catch (Exception e) {
		writeln("Exception");
		throw e;
		return 2;
	}

	return 0;
}