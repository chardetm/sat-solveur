﻿module user_io.interactive_mode;

import std.stdio, std.string;

enum imChoice_t {NONE, GRAPH, NEXT_CONFLICT, TERMINATE};

imChoice_t getUserChoice () {
	string answer;
	write("Mode interactif. Sélectionnez votre choix (g/c/t) : ");
	answer = readln();
	answer = chomp(toLower(answer));
	while (answer != "g" && answer != "c" && answer != "t") {
		write("Choix non reconnu. Sélectionnez votre choix (g/c/t) : ");
		answer = readln();
		answer = chomp(toLower(answer));
	}
	if (answer == "g") {
		return imChoice_t.GRAPH;
	} else if (answer == "c") {
		return imChoice_t.NEXT_CONFLICT;
	} else  {
		return imChoice_t.TERMINATE;
	}
}
