﻿module user_io.arguments_handler;

import std.stdio, std.exception, std.getopt, std.conv;
import types.basic_types;

class SyntaxError : Exception {
	this () {
		super("");
	}
	this (string s) {
		super(s);
	}
}

private void singleToDoubleDash (ref string[] args) {
	foreach (ref s; args) {
		if (s.length>2 && s[0] == '-' && s[1] != '-') {
			s = '-'~s;
		}
	}
}

mode_t handleArguments (ref string[] args, out ulong nbColors, out format_t format, out pari_t choixPari, out bool coq) {
	bool cl, cli, explain;//EXTERMINAAAAAAATE!
	bool othfor;
	bool rand, moms, dlis, vsids;
	coq = false;
	mode_t choice = mode_t.NORMAL;
	nbColors=0;
	
	try {
		singleToDoubleDash(args);
		getopt(args, "cl", &cl, "cl-interact", &cli, "color", &nbColors, "explainunsat", &explain, "tseitin", &othfor,
			"rand", &rand, "moms", &moms, "dlis", &dlis, "vsids", &vsids, "coq", &coq);
	} catch (ConvException e) {
		throw new SyntaxError;
	} catch (Exception e) {
		throw new SyntaxError;
	}

	if(args.length < 2) { // Input file missing
		throw new SyntaxError;
	}
	
	if(othfor)
		format = format_t.OTHER;

	if(cli) {
		if (nbColors != 0) { // Both color and cl-interact incompatible flags given
			throw new SyntaxError;
		}
		if (cl) writeln("Warning: cl and cl-interact flags both given, using cl-interact.");
		choice = mode_t.CL_INTERAC;

	} else if(cl) {
		if (nbColors != 0) {
			throw new SyntaxError;
		}
		choice = mode_t.CL;

	} else if(nbColors != 0) {
		choice = mode_t.COLOR;
		if(nbColors > 11) {
			writeln("Pas assez de couleurs disponibles. Si vraiment c'est nécessaire, envoyer un mail pour qu'on fasse un générateur aléatoire...");
			throw new SyntaxError("Pas assez de couleurs en stock !");
		}
	} else if(explain) {
		choice = mode_t.EXPLAIN;
	}
	if(rand)
	{
		choixPari = pari_t.RAND;
	}
	else if(moms)
	{
		choixPari = pari_t.MOMS;
	}
	else if(dlis)
	{
		choixPari = pari_t.DLIS;
	}
	else if(vsids)
	{
		choixPari = pari_t.VSIDS;
		if(!cl && !cli)
		{
			writeln("VSIDS ne marche qu'avec clause learning. Mode cl activé.");
			choice = mode_t.CL;
		}
	}
	else
		choixPari = pari_t.NORMAL;

	return choice;
}
