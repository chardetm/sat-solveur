﻿module solveurs.solveur;

public import types.basic_types, types.valuation;

interface Solveur {

	/**
	 *  Renvoie si, en l'état actuel de la valuation, le problème peut avoir une solution vis à vis de la théorie.
	 */
	bool stillSolvable ();

	// Pour plus tard :
	// lit_t[] whyNotSolvable (); <- null si résolvable

	/**
	 *  Marque quand un pari a été fait (à chaque changement de niveau) pour le backtrack.
	 *  Renvoie faux ssi une contradiction a été trouvée.
	 */
	bool markPari ();
	
	/**
	 *  Marque quand un changement de variable a été fait (suite à un pari).
	 *  Renvoie faux ssi une contradiction a été trouvée.
	 */
	bool markChangeVar ();

	/** 
	 *  Backtrack d'un seul niveau (les niveaux sont mémorisés lors de l'appel de markPari).
	 */
	void backtrack();

	/**
	 *  Backtrack jusqu'au niveau level (les niveaux sont mémorisés lors de l'appel de markPari).
	 */
	void backtrackToLevel (ulong level);



	/**
	 *  Appel de DPLL, retourne vrai ssi le problème a une solution.
	 */
	void solve ();

	/**
	 *  Retourne une valuation d'entiers permettant de satisfaire le problème du point de vue de la théorie.
	 *  /!\ DPLL DOIT AVOIR REPONDU POUR QUE LA VALUATION RENVOYE AIT UN SENS! /!\
	 */
	valth_t[] valuation ();

}
