module solveurs.sol_con;

import solveurs.solveur, types.arbre,types.instance, types.union_find, types.clause;
import types.termeCon;
import std.typecons, std.conv, std.stdio, std.ascii;

version(DPLL)
{
	import algorithms.dpll_theorie;
}
version(watchedLiterals)
{
	import algorithms.watched_literals_theorie;
}

alias varOrNum = Tuple!(ulong, "level", var_t, "num");

class SolveurCon : Solveur
{
protected:
	Arbre[] _atomeToVar; //fais le lien entre un atome et une variable dpll
	Arbre _formuleDpll; //nouvelle formule avec des variables DPLL (x_1 = x_2 /\ x_3 = x_4 devient 1 /\ 2)
	UnionFind _repTheor;
	varOrNum[][] _difference;
	long[] _val;
	var_t _varMax; // Dernière variable "normale" (les autres sont des variables représentant des points précis des fonctions)
	ulong _levelActuel;
	UnionFind[] _souvenir;
	Valuation _valu;
	pari_t _choixPari;
	TermeCongruence[] _listTerme; // Contient les termes correspondant aux variables > _varMax
	mode_t _mode;

public:
	this(Arbre a, pari_t choix, mode_t mode = mode_t.NORMAL)
	{
		_choixPari  = choix;
		_varMax = a.varMax;
		int nexV = cast(int)_varMax + 1;
		constructListTerm(a, nexV);
		int nexVarDispo = 1;
		_difference.length = _varMax + _listTerme.length + 1;
		_val.length = _varMax + _listTerme.length + 1;
		_atomeToVar.length = 1;
		constructProblem(a, _atomeToVar, _formuleDpll, nexVarDispo);
		_levelActuel = 0;
		_repTheor = new UnionFind(_varMax + _listTerme.length);
		_souvenir ~= new UnionFind(_repTheor);
		_mode = mode;
	}

	static int addUnique(ref Arbre[] l, Arbre e, ref int nexVarDispo)
	{
		int i = 0;
		while(i < l.length)
		{
			if(l[i] == e)
			{
				return i;
			}
			i++;
		}
		l ~= e;
		nexVarDispo++;
		return (nexVarDispo - 1 );
	}

	int addUniqueTerme(ref TermeCongruence[] l, TermeCongruence t, ref int nexVarDispo)
	{
		int i = 0;
		while(i < l.length)
		{
			if(l[i] == t)
			{
				return i + cast(int)_varMax + 1;
			}
			i++;
		}
		l ~= t;
		nexVarDispo++;
		return (nexVarDispo - 1 );
	}

	//transforme les atomes en variables
	void constructProblem(Arbre a, ref Arbre[] l, ref Arbre res, ref int nexVarDispo)
	{
		if(a.etiquette == "=")
		{
			res = new Arbre(to!string(addUnique(l, a, nexVarDispo)));
		}
		else
		{
			res = new Arbre(a.etiquette);
			foreach(e; a.lFils())
			{
				Arbre te = new Arbre();
				constructProblem(e, l, te, nexVarDispo);
				res.addFils(te);
			}
		}
	}

	//transforme les termes en variable du problème, et fait la liste des termes
	TermeCongruence constructListTerm(ref Arbre a, ref int nexVarDispo)
	{
		if(!isFunction(a.etiquette))
		{
			foreach(ref e; a.lFils)
			{
				constructListTerm(e, nexVarDispo);
			}
			return null;
		}
		else if(a.etiquette == "x")
		{
			TermeCongruence cur = new TermeCongruence("x" ~ to!string(a.fils(0).etiquette), 0);
			return cur;
		}
		else
		{
			TermeCongruence cur = new TermeCongruence(a.etiquette, a.length);
			foreach(ref e; a.lFils)
			{
				TermeCongruence t = constructListTerm(e, nexVarDispo);
				cur.addArg(t);
			}
			int ind = addUniqueTerme(_listTerme, cur, nexVarDispo);
			a = new Arbre("x");
			a.addFils(new Arbre(to!string(ind)));
			return cur;
		}
	}

	bool isFunction(string name)
	{
		switch(name)
		{
			case "\\\\/", "/\\\\", "=", "~":
				return false;
			default:
				return name.length > 0 && !isDigit(name[0]);
		}
	}

	bool stillSolvable ()
	{
		foreach(i;1.._val.length)
		{
			if(!verif(i))
			{
				return false;
			}
		}
		return true;
	}

	bool markPari ()
	{
		var_t variable = _valu.dernierChVar();
		if(variable >= _atomeToVar.length)
		{
			//dpll a changé une variable "en plus" (ie qui a été ajouté pour tseitin)
			//n'est pas un vrai changement d'atome
			return true;
		}
		bool val = _valu.getVal(variable) == Vrai;
		_souvenir ~= new UnionFind(_repTheor);
		_levelActuel++;
		process(_atomeToVar[variable], val);
		return stillSolvable;
	}
	
	bool markChangeVar ()
	{
		var_t variable = _valu.dernierChVar();
		if(variable >= _atomeToVar.length)
		{
			//dpll a changé une variable "en plus" (ie qui a été ajouté pour tseitin)
			//n'est pas un vrai changement d'atome
			return true;
		}
		bool val = _valu.getVal(variable) == Vrai;
		process(_atomeToVar[variable], val);
		return stillSolvable;
	}

	void backtrack()
	{
		backtrackToLevel(_valu.etage-1);
	}

	void backtrackToLevel (ulong level)
	{
		_repTheor = _souvenir[$-(_levelActuel-level)];
		_souvenir.length -= _levelActuel-level;
		_levelActuel = level;
		foreach(i;1.._difference.length)
		{
			varOrNum[] temp;
			foreach(e;_difference[i])
			{
				if(e.level <= _levelActuel)
					temp ~= e;
			}
			_difference[i] = temp;
		}
	}

	void solve ()
	{
		Instance cnfProb = _formuleDpll.transfoTseitin(_choixPari, _mode);

		version(DPLL)
		{
			InstanceDPLLTh inst = new InstanceDPLLTh(cnfProb, this);
		}
		version(watchedLiterals)
		{
			InstanceWLTheo inst = new InstanceWLTheo(cnfProb, this);
		}
		_valu = inst._val;
		inst.algo();

		if(!stillSolvable) {
			writeln("Unexpected error: theory says not solvable but DPLL(T) says it is...");
			throw new NotSolvable("Error!");
		}
		endProcess();

		writeln("s SATISFIABLE");

		// Affichage de la valuation pour la théorie
		foreach(i;1.._val.length)
		{
			if (i <= _varMax) {
				write("x", i);
			} else {
				write(_listTerme[i-_varMax-1].toString(_val));
			}
			write("=", _val[i], " ");
		}
		writeln("0");

		debug {
			writeln("Valuation DPLL(T) : ", _valu);
		}
	}

	valth_t[] valuation ()
	{
		return _val;
	}

	void process(Arbre a, bool egalite)
	{
		if(egalite)
		{
			var_t v = _repTheor.unionF(find(to!int(a.fils(0).fils(0).etiquette)),find(to!int(a.fils(1).fils(0).etiquette)));
			var_t au = v == find(to!int(a.fils(0).fils(0).etiquette)) ? to!int(a.fils(1).fils(0).etiquette) : to!int(a.fils(0).fils(0).etiquette);
			foreach(e;_difference[au])
			{
				_difference[v] ~= varOrNum(_levelActuel, e.num);
			}
			foreach(i;0.._listTerme.length)
			{
				foreach(j;i+1.._listTerme.length)
				{
					//si deux termes sont maintenant égaux, on précise qu'ils ont le même représentant
					//permet de détecter des erreurs
					if(_listTerme[i].egal(_listTerme[j], _repTheor, _listTerme, _varMax))
					{
						v = _repTheor.unionF(i+_varMax+1, j+_varMax+1);
						au = v == find(i+_varMax+1) ? j+_varMax + 1 : i+_varMax + 1;
						foreach(e;_difference[au])
						{
							_difference[v] ~= varOrNum(_levelActuel, e.num);
						}
					}
				}
			}
		}
		else
		{
			_difference[to!int(a.fils(0).fils(0).etiquette)]~= varOrNum(_levelActuel,to!int(a.fils(1).fils(0).etiquette));
			_difference[to!int(a.fils(1).fils(0).etiquette)]~= varOrNum(_levelActuel,to!int(a.fils(0).fils(0).etiquette));
		}
	}

	bool verif(var_t v)
	{
		foreach(e; _difference[v])
		{
			if(egal(v, e.num))
			{
				return false;
			}
		}
		return true;
	}

	var_t find(var_t v)
	{
		return _repTheor.find(v);
	}

	//trouve les affectation
	void endProcess()
	{
		long current = 1;
		foreach(i;1.._val.length)
		{
			if(find(i) == i)
			{
				_val[i] = current;
				current++;
			}
		}
		foreach(i;1.._val.length)
		{
			_val[i] = _val[find(i)];
		}
	}
	bool egal(var_t v1, var_t v2)
	{
		if(v1 <= _varMax || v2 <= _varMax)
		{
			return find(v1)==find(v2);
		}
		else
		{
			return _listTerme[v1-_varMax-1].egal(_listTerme[v2-_varMax-1], _repTheor, _listTerme, _varMax);
		}
	}
}
