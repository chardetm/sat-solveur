module solveurs.sol_ega;

import solveurs.solveur, types.arbre, types.instance, types.union_find, types.clause;
import std.typecons, std.conv, std.stdio;
import coq.interface_ega;

version(DPLL)
{
	import algorithms.dpll_theorie;
}
version(watchedLiterals)
{
	import algorithms.watched_literals_theorie;
}

alias varOrNum = Tuple!(ulong, "level", var_t, "num");

class SolveurEga : Solveur
{
protected:
	Arbre[] _atomeToVar; //fais le lien entre un atome et une variable dpll
	Arbre _formuleDpll; //nouvelle formule avec des variables DPLL (x_1 = x_2 /\ x_3 = x_4 devient 1 /\ 2)
	UnionFind _repTheor;
	varOrNum[][] _difference;
	long[] _val;
	var_t _varMax;
	ulong _levelActuel;
	UnionFind[] _souvenir;
	Valuation _valu;
	pari_t _choixPari;
	bool _coq;
	mode_t _mode;

public:
	this(Arbre a, pari_t choix, mode_t mode = mode_t.NORMAL, bool coq = false)
	{
		_coq = coq;
		_choixPari  = choix;
		_varMax = a.varMax;
		_repTheor = new UnionFind(_varMax);
		int nexVarDispo = 1;
		_difference.length = _varMax+1;
		_val.length = _varMax + 1;
		_atomeToVar.length = 1;
		constructProbleme(a,_atomeToVar, _formuleDpll, nexVarDispo);
		_levelActuel = 0;
		_souvenir ~= _repTheor;
		_coq = coq;
		_mode = mode;
	}

	bool stillSolvable ()
	{
		bool rep = true;
		foreach(i;1.._difference.length)
		{
			foreach(e;_difference[find(i)])
			{
				rep = rep && find(i) != find(e.num);
			}
		}
		return rep;
	}

	// Pour plus tard :
	// lit_t[] stillSolvableR (); <- null si résolvable

	bool markPari ()
	{
		var_t variable = _valu.dernierChVar();
		if(variable >= _atomeToVar.length)
		{
			//dpll a changé une variable "en plus" (ie qui a été ajouté pour tseitin)
			//n'est pas un vrai changement d'atome
			return true;
		}
		bool val = _valu.getVal(variable) == Vrai;
		_souvenir ~= new UnionFind(_repTheor);
		_levelActuel++;
		process(_atomeToVar[variable], val);
		var_t v1 = to!var_t(_atomeToVar[variable].fils(0).fils(0).etiquette);
		var_t v2 = to!var_t(_atomeToVar[variable].fils(1).fils(0).etiquette);
		bool rep = true;
		foreach(e; _difference[find(v1)])
		{
			rep = rep && find(v1) != find(e.num);
		}
		foreach(e; _difference[find(v2)])
		{
			rep = rep && find(v2) != find(e.num);
		}
		return rep;
	}

	void backtrack()
	{
		backtrackToLevel(_valu.etage-1);
	}

	void backtrackToLevel (ulong level)
	{
		_repTheor = new UnionFind(_souvenir[$-(_levelActuel-level)]);
		_souvenir.length -= _levelActuel-level;
		_levelActuel = level;
		foreach(i;1.._difference.length)
		{
			varOrNum[] temp;
			foreach(e;_difference[i])
			{
				if(e.level <= _levelActuel)
				{
					temp ~= e;
				}
			}
			_difference[i] = temp;
		}
	}
	
	bool markChangeVar()
	{
		var_t variable = _valu.dernierChVar();
		if(variable >= _atomeToVar.length)
		{
			//dpll a changé une variable "en plus" (ie qui a été ajouté pour tseitin)
			//n'est pas un vrai changement d'atome
			return true;
		}
		bool val = _valu.getVal(variable) == Vrai;
		process(_atomeToVar[variable], val);
		var_t v1 = to!var_t(_atomeToVar[variable].fils(0).fils(0).etiquette);
		var_t v2 = to!var_t(_atomeToVar[variable].fils(1).fils(0).etiquette);
		bool rep = true;
		foreach(e; _difference[find(v1)])
		{
			rep = rep && find(v1) != find(e.num);
		}
		foreach(e; _difference[find(v2)])
		{
			rep = rep && find(v2) != find(e.num);
		}
		return rep;
	}

	void solve ()
	{
		Instance cnfProb = _formuleDpll.transfoTseitin(_choixPari, _mode);

		version(DPLL)
		{
			InstanceDPLLTh inst = new InstanceDPLLTh(cnfProb, this);
		}
		version(watchedLiterals)
		{
			InstanceWLTheo inst = new InstanceWLTheo(cnfProb, this);
		}
		_valu = inst._val;
		try
		{
			inst.algo();
			endProcess();

			if(!stillSolvable()) {
				writeln("Unexpected error: theory says not solvable but DPLL(T) says it is...");
				throw new NotSolvable("Error!");
			}

			writeln("s SATISFIABLE");

			// Affichage de la valuation pour la théorie
			foreach(i;1.._val.length)
			{
				write("x", i, "=", _val[i], " ");
			}
			writeln("0");

			debug {
				writeln("Valuation DPLL(T) : ", _valu);
			}
			InterfaceDCoqEga inter = new InterfaceDCoqEga(_formuleDpll, _atomeToVar, _valu, _val, true);
			inter.coqProof();
		}
		catch(NotSolvable e)
		{
			InterfaceDCoqEga inter = new InterfaceDCoqEga(_formuleDpll, _atomeToVar, _valu, _val, false);
			inter.coqProof();
			throw e;
		}
	}

	valth_t[] valuation ()
	{
		endProcess();
		return _val;
	}

	//on change les atomes en variable
	void constructProbleme(Arbre a, ref Arbre[] l, ref Arbre res, ref int nexVarDispo)
	{
		if(a.etiquette == "=")
		{
			res = new Arbre(to!string(addUnique(l, a, nexVarDispo)));
		}
		else
		{
			res = new Arbre(a.etiquette);
			foreach(e; a.lFils())
			{
				Arbre te = new Arbre();
				constructProbleme(e, l, te, nexVarDispo);
				res.addFils(te);
			}
		}
	}

	static int addUnique(ref Arbre[] l, Arbre e, ref int nexVarDispo)
	{
		int i = 0;
		while(i < l.length)
		{
			if(l[i] == e)
			{
				return i;
			}
			i++;
		}
		l ~= e;
		nexVarDispo++;
		return (nexVarDispo - 1 );
	}

	void process(Arbre a,bool egalite)
	{
		if(egalite)
		{
			var_t v = _repTheor.unionF(find(to!int(a.fils(0).fils(0).etiquette)),find(to!int(a.fils(1).fils(0).etiquette)));
			var_t au = (v == find(to!int(a.fils(0).fils(0).etiquette))) ? to!int(a.fils(1).fils(0).etiquette) : to!int(a.fils(0).fils(0).etiquette);
			foreach(e;_difference[au])
			{
				//Grâce ça, le représentant connait toutes les variables qui sont différentes
				//ie : si x1 = x2, x2 != x3 et que x1 est le représentant de x2, alors x1 sait que x1 != x3
				_difference[v] ~= varOrNum(_levelActuel, find(e.num));
			}
		}
		else
		{
			_difference[find(to!int(a.fils(0).fils(0).etiquette))]~= varOrNum(_levelActuel,find(to!int(a.fils(1).fils(0).etiquette)));
			_difference[find(to!int(a.fils(1).fils(0).etiquette))]~= varOrNum(_levelActuel,find(to!int(a.fils(0).fils(0).etiquette)));
		}
	}

	//trouve les affectation
	void endProcess()
	{
		long current = 1;
		foreach(i;1.._varMax+1)
		{
			if(find(i) == i)
			{
				//on donne des valeurs aux représentants
				_val[i] = current;
				current++;
			}
		}
		foreach(i;1.._varMax+1)
		{
			//puis on transmet
			_val[i] = _val[find(i)];
		}
	}

	var_t find(var_t v)
	{
		return _repTheor.find(v);
	}

	Arbre[] atomeToVar()
	{
		return _atomeToVar.dup;
	}
	Valuation val()
	{
		return _valu;
	}
	Arbre formuleDpll()
	{
		return _formuleDpll;
	}

}
