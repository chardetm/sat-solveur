module solveurs.sol_dif;

import solveurs.solveur, types.arbre, types.instance, types.union_find, types.clause;
import types.grapheDif, types.entier;
import std.typecons, std.conv, std.stdio;
version(DPLL)
{
	import algorithms.dpll_theorie;
}
version(watchedLiterals)
{
	import algorithms.watched_literals_theorie;
}

alias intervalle = Tuple!(Entier, "a", Entier, "b"); //[a;b]

class SolveurDif : Solveur
{
protected:
	Arbre[] _atomeToVar;
	Arbre _formuleDPLL;
	GrapheDif _graphe;
	long[] _val;
	intervalle[][] _possible; //premier : variable deuxième : level
	var_t _varMax;
	ulong _levelActuel;
	GrapheDif[] _souvenir;
	Valuation _valu;
	pari_t _choixPari;
	mode_t _mode;

public:
	this(Arbre a, pari_t choix, mode_t mode = mode_t.NORMAL)
	{
		_choixPari  = choix;
		_varMax = a.varMax();
		_atomeToVar.length = 1;
		_graphe = new GrapheDif(_varMax);
		_val.length = _varMax+1;
		_levelActuel = 0;
		_souvenir ~= new GrapheDif(_graphe);
		_possible.length = _varMax + 1;
		for(int i = 1; i <= _varMax; i++)
		{
			_possible[i] ~= intervalle(Entier(-1, true), Entier(1, true));
		}
		int nexVarDispo = 1;
		constructProblem(a, _atomeToVar, _formuleDPLL, nexVarDispo);
		_mode = mode;
	}

	//constructProblem remplace les atome en variables
	void constructProblem(Arbre a, ref Arbre[] l, ref Arbre res, ref int nexVarDispo)
	{
		if(a.etiquette == "<=")
		{
			res = new Arbre(to!string(addUnique(l, a, nexVarDispo)));
		}
		else
		{
			res = new Arbre(a.etiquette);
			foreach(e; a.lFils())
			{
				Arbre te = new Arbre();
				constructProblem(e, l, te, nexVarDispo);
				res.addFils(te);
			}
		}
	}
	static int addUnique(ref Arbre[] l, Arbre e, ref int nexVarDispo)
	{
		int i = 0;
		while(i < l.length)
		{
			if(l[i] == e)
			{
				return i;
			}
			i++;
		}
		l ~= e;
		nexVarDispo++;
		return (nexVarDispo - 1 );
	}

	bool stillSolvable()
	{
		debug(2)
		{
			writeln(_valu);
		}
		bool rep = !_graphe.cycleAbsorbant();
		//si cycleAbsorbant, on a donc xi <= xi - n avec n > 0 : ça risque de poser problème
		//il faut quand même vérifier ensuite que les intervalles soient possible
		//ie : si x1 >= 5, x2 >= x1 et x2 < 5, il n'y a pas de cycle absorbant mais l'intervalle de x2 sera [5,4]
		//donc pas possible
		for(int i = 1; i <= _varMax && rep; i++)
		{
			intervalle I = _possible[i][_levelActuel];
			debug(2)
			{
				writeln("x", i, " in [", I.a,";",I.b,"]");
			}
			for(int j = 1; j <= _varMax; j++)
			{
				if(_graphe.arretePres(i,j))
				{
					Entier p = _graphe.arretePoid(i,j);
					//xi <= xj + p donc si xi \in [ai, bi] et xj \in [aj,bj] alors xi \in [ai, min(bi,bj+p)]
					I.b = min(_possible[j][_levelActuel].b + p, I.b);
				}
				if(_graphe.arretePres(j,i))
				{
					Entier p = _graphe.arretePoid(j,i);
					//xj - p <= xi donc si xi \in [ai, bi] et xj \in [aj,bj] alors xi \in [max(ai,aj-p), bi]
					I.a = max(_possible[j][_levelActuel].a - p, I.a);
				}
			}
			debug(2)
			{
				writeln("x", i, " in [", I.a,";",I.b,"]");
			}
			rep = I.a <= I.b;
		}
		return rep;
	}
	bool markPari()
	{
		var_t variable = _valu.dernierChVar();
		if(variable >= _atomeToVar.length)
		{
			//dpll a changé une variable "en plus" (ie qui a été ajouté pour tseitin)
			//n'est pas un vrai changement d'atome
			return true;
		}
		bool val = _valu.getVal(variable) == Vrai;
		_souvenir ~= new GrapheDif(_graphe);
		_levelActuel++;
		for(int i = 1; i <= _varMax; i++)
		{
			_possible[i] ~= _possible[i][$-1];//nouvel etage pour les intervalles
		}
		process(_atomeToVar[variable], val);
		return stillSolvable;
	}
	bool markChangeVar()
	{
		var_t variable = _valu.dernierChVar();
		if(variable >= _atomeToVar.length)
		{
			//dpll a changé une variable "en plus" (ie qui a été ajouté pour tseitin)
			//n'est pas un vrai changement d'atome
			return true;
		}
		bool val = _valu.getVal(variable) == Vrai;
		process(_atomeToVar[variable], val);
		return stillSolvable;
	}

	void solve() {
		Instance cnfProb = _formuleDPLL.transfoTseitin(_choixPari, _mode);
		version(DPLL)
		{
			InstanceDPLLTh inst = new InstanceDPLLTh(cnfProb, this);
		}
		version(watchedLiterals)
		{
			InstanceWLTheo inst = new InstanceWLTheo(cnfProb, this);
		}

		_valu = inst._val;
		inst.algo();
		endProcess();

		if(!stillSolvable()) {
			writeln("Unexpected error: theory says not solvable but DPLL(T) says it is...");
			throw new NotSolvable("Error!");
		}

		writeln("s SATISFIABLE");

		// Affichage de la valuation pour la théorie
		foreach(i; 1.._val.length)
		{
			write("x", i, "=", _val[i], " ");
		}
		writeln("0");

		debug {
			writeln("Valuation DPLL(T) : ", _valu);
		}
	}

	void backtrack()
	{
		backtrackToLevel(_valu.etage-1);
	}
	void backtrackToLevel(ulong level)
	{
		_graphe = new GrapheDif(_souvenir[$-(_levelActuel-level)]);
		_souvenir.length -= _levelActuel - level;
		_levelActuel = level;
		for(int i = 1; i <= _varMax; i++)
		{
			_possible[i].length = level+1;
		}
	}
	long[] valuation()
	{
		return _val.dup;
	}

	void process(Arbre a, bool val)
	{
		if(a.fils(0).etiquette == "-")
		{
			//atome de la forme xi - xj <= p
			//on recupére donc i, j et p
			var_t a1 = to!ulong(a.fils(0).fils(0).fils(0).etiquette);
			var_t a2 = to!ulong(a.fils(0).fils(1).fils(0).etiquette);
			long poid = to!long(a.fils(1).etiquette);

			if(val)
			{
				// on a bien xi - xj <= p
				_graphe.addArrete(a1, a2, poid);
			}
			else
			{
				// ~(xi <= xj + n) -> xi > xj + n -> xi >= xj + n + 1 -> xj <= xi - (n+1)
				_graphe.addArrete(a2, a1, -(poid+1));
			}
		}
		else
		{
			//atome de la forme xi <= p
			//on recupére i et p
			var_t a1 = to!ulong(a.fils(0).fils(0).etiquette);
			long poid = to!long(a.fils(1).etiquette);
			if(val)
			{
				//on a bien xi <= p
				if(_possible[a1][_levelActuel].b > poid)
				{
					_possible[a1][_levelActuel].b = poid;
				}
			}
			else
			{
				poid += 1;// ~(xi <= n) -> xi > n -> xi >= n + 1
				if(_possible[a1][_levelActuel].a < poid)
				{
					_possible[a1][_levelActuel].a = poid;
				}
			}
		}
	}
	//trouve les affectation
	void endProcess()
	{
		debug(2)
		{
			writeln("END PROCESS");
		}
		for(int i = 1; i <= _varMax; i++)
		{
			intervalle I = _possible[i][_levelActuel];
			//I est un sous intervalle de _possible[i][_levelActuel]
			//Il est construit en fonction du graphe
			debug(2)
			{
				writeln("x", i, " in [", I.a,";",I.b,"]");
			}
			for(int j = 1; j <= _varMax; j++)
			{
				if(_graphe.arretePres(i,j))
				{
					Entier p = _graphe.arretePoid(i,j);
					//xi <= xj + p donc si xi \in [ai, bi] et xj \in [aj,bj] alors xi \in [ai, min(bi,bj+p)]
					I.b = min(_possible[j][_levelActuel].b + p, I.b);
				}
				if(_graphe.arretePres(j,i))
				{
					Entier p = _graphe.arretePoid(j,i);
					//xj - p <= xi donc si xi \in [ai, bi] et xj \in [aj,bj] alors xi \in [max(ai,aj-p), bi]
					I.a = max(_possible[j][_levelActuel].a - p, I.a);
				}
			}
			if(I.a.inf)
			{
				if(I.b.inf)
				{
					//I = [-inf; +inf] toute valeur est possible
					_val[i] = 0;
				}
				else
				{
					_val[i] = I.b.val;
				}
			}
			else
			{
				_val[i] = I.a.val;
			}
			debug(2)
			{
				writeln("x", i, " in [", I.a,";",I.b,"]");
			}
			//vu que xi = _val[i], l'intervalle possible devient [_val[i], _val[i]].	
			_possible[i][_levelActuel] = intervalle(Entier(_val[i], false), Entier(_val[i], false));
		}

	}
}

Entier min(Entier a, Entier b)
{
	return a < b ? a : b;
}
Entier max(Entier a, Entier b)
{
	return a < b ? b : a;
}
