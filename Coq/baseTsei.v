Require Import Arith.

Inductive Val : Set :=
|EV : Val
|Faux : Val -> Val
|Vrai : Val -> Val.

Fixpoint getVal V n := match V, n with
|EV, _ => False
|Vrai _, 0 => True
|Faux _, 0 => False
|Vrai V1, S k => getVal V1 k
|Faux V1, S k => getVal V1 k
end.

Inductive Formule : Set :=
|Or : Formule -> Formule -> Formule
|And : Formule -> Formule -> Formule
|Neg : Formule -> Formule
|Var : nat -> Formule.

Notation "A /_\ B" := (And A B) (at level 50).
Notation "A \_/ B" := (Or A B) (at level 55).
Notation "~_ A" := (Neg A) (at level 60).
Notation "# n" := (Var n) (at level 45).

Fixpoint valu F V := match F with
|A /_\ B => valu A V /\ valu B V
|A \_/ B => valu A V \/ valu B V
|~_A => ~(valu A V)
|Var x => getVal V x
end.

Definition sat F := exists V, valu F V.
