module coq.interface_tsei;

import std.stdio, std.file, std.string, std.conv;
import types.basic_types, types.arbre, types.valuation;

enum base = "Coq/baseTsei.v";

class InterfaceTsei
{
protected:
	bool _sat;
	Valuation _valu;
	File _file;
	Arbre _formule;

public:
	this(Arbre formule, Valuation val,bool sat = true, string fileName = "preuve")
	{
		_sat = sat;
		_formule = formule;
		_valu = val;
		_file = File(fileName ~ ".v", "w");
	}

	void copyBase()
	{
		auto base = File(base, "r");
		string line;
		while((line = base.readln()) != null)
		{
			_file.write(line);
		}
	}
	void coqProof()
	{
		copyBase();
		suite();
	}
	void suite()
	{
		_file.writeln();
		_file.writeln("Definition F := " ~ fToString(_formule) ~ ".");

		if(_sat)
		{
			string V;
			string parFer;
			foreach(i;1.._formule.entierMax() + 1)
			{
				if(_valu.getVal(i) == Vrai)
					V ~= "Vrai ";
				else
					V ~= "Faux ";
				V ~= " (";
				parFer ~= ")";
			}
			_file.writeln("Definition V := " ~ V ~ "EV" ~ parFer ~ ".");

			_file.writeln();
			_file.writeln("Lemma satF : sat F.");
			proofTrue();
			_file.writeln("Qed.");

			_file.writeln();
			_file.writeln("Check satF.");
		}
		else
		{
			_file.writeln();
			_file.writeln("Lemma nSatF : ~(sat F).");
			proofFalse();
			_file.writeln("Qed.");

			_file.writeln();
			_file.writeln("Check nSatF.");
		}
	}

	void proofTrue()
	{
		_file.writeln("exists V.");
		_file.writeln("simpl.");
		_file.writeln("firstorder.");
	}
	void proofFalse()
	{
		_file.writeln("intro.");
		_file.writeln("destruct H as (V&val).");
		valu_t[] V;
		V.length = _formule.entierMax() + 1;
		V[] = Inconnu;
		int i = 1;
		while(i != 0)
		{
			if(i <= V.length -1)
			{
				if(V[i] == Inconnu)
				{
					_file.writeln("destruct V.");
					_file.writeln("simpl in val.");
					_file.writeln("firstorder.");
					V[i] = Faux;
					i++;
				}
				else if(V[i] == Faux)
				{
					V[i] = Vrai;
					i++;
				}
				else
				{
					V[i] = Inconnu;
					i--;
				}
			}
			else
			{
				_file.writeln("simpl in val.");
				_file.writeln("firstorder.");
				i--;
			}
		}
	}

	string fToString(Arbre a)
	{
		if(a.etiquette == "/\\\\")
		{
			return "(" ~ fToString(a.fils(0)) ~"/_\\" ~ fToString(a.fils(1)) ~ ")";
		}
		else if(a.etiquette == "\\\\/")
		{
			return "(" ~ fToString(a.fils(0)) ~"\\_/" ~ fToString(a.fils(1)) ~ ")";
		}
		else if(a.etiquette == "~")
		{
			return "(~_" ~ fToString(a.fils(0)) ~ ")";
		}
		else
		{
			return "#" ~ (to!string(to!int(a.etiquette) - 1));
		}
	}

	bool satisf(Arbre a, valu_t[] valu)
	{
		if(a.etiquette == "/\\\\")
		{
			return satisf(a.fils(0), valu) && satisf(a.fils(1), valu);
		}
		else if(a.etiquette == "\\\\/")
		{
			return satisf(a.fils(0), valu) || satisf(a.fils(1), valu);
		}
		else if(a.etiquette == "~")
		{
			return !satisf(a.fils(0), valu);
		}
		else
		{
			return valu[to!ulong(a.etiquette)] == Vrai;
		}
	}
}