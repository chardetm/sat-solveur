Require Import Arith.

Inductive Val : Set :=
|EV : Val
|Faux : Val -> Val
|Vrai : Val -> Val.

Inductive ValEnt : Set :=
|EVE : ValEnt
|NV : nat -> ValEnt -> ValEnt.

Fixpoint getVal V n := match V, n with
|EV, _ => False
|Vrai _, 0 => True
|Faux _, 0 => False
|Vrai V1, S k => getVal V1 k
|Faux V1, S k => getVal V1 k
end.

Fixpoint getValEnt V n := match V, n with
|EVE, _ => 0
|NV k V1, 0 => k
|NV k V1, S m => getValEnt V1 m
end.

Inductive AtomeList : Set :=
|EA : AtomeList
|NVA : nat -> nat -> AtomeList -> AtomeList.

Fixpoint length V := match V with
|EV => 0
|Faux l => 1 + length l
|Vrai l => 1 + length l
end.

Fixpoint lengthA l := match l with
|EA => 0
|NVA _ _ l1 => 1 + lengthA l1
end.

Inductive Formule : Set :=
|Or : Formule -> Formule -> Formule
|And : Formule -> Formule -> Formule
|Neg : Formule -> Formule
|Var : nat -> Formule.

Notation "A /_\ B" := (And A B) (at level 50).
Notation "A \_/ B" := (Or A B) (at level 55).
Notation "~_ A" := (Neg A) (at level 60).
Notation "# n" := (Var n) (at level 45).

Fixpoint valu F V := match F with
|A /_\ B => valu A V /\ valu B V
|A \_/ B => valu A V \/ valu B V
|~_A => ~(valu A V)
|Var x => getVal V x
end.

Fixpoint valuPossible F V := match F with
|A /_\ B => valuPossible A V /\ valuPossible B V
|A \_/ B => valuPossible A V /\ valuPossible B V
|~_A => valuPossible A V
|Var x => x <= length V
end.

Fixpoint valuEnt listAt V VE:= match listAt, V with
|EA, EV => True
|EA, Vrai _ => False
|EA, Faux _ => False
|NVA _ _ _, EV => False
|NVA a b l, Faux s => (getValEnt VE a <> getValEnt VE b) /\ valuEnt l s VE
|NVA a b l, Vrai s => (getValEnt VE a = getValEnt VE b) /\ valuEnt l s VE
end.

Lemma natPositif : forall n, n >= 0.
induction n.
constructor.
constructor.
apply IHn.
Qed.

Definition sat F listAt := exists V, length V = lengthA listAt /\ valu F V /\ (exists VE, valuEnt listAt V VE).
