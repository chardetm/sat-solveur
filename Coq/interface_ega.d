module coq.interface_ega;

import std.stdio, std.file, std.string, std.conv;
import types.basic_types, types.arbre, types.valuation, coq.graphe, types.union_find;

enum base = "Coq/baseEga.v";

class InterfaceDCoqEga
{
protected:
	bool _sat;
	Arbre _formule;
	Arbre[] _varToAtome;
	File _file;
	Valuation _valu;
	long[] _valuEnt;


public:
	this(Arbre formule, Arbre[] varToAtome, Valuation val, long[] valuEnt,bool sat = true, string fileName = "preuve")
	{
		_formule = formule;
		_varToAtome = varToAtome;
		_file = File(fileName ~ ".v", "w");
		_sat = sat;
		_valu = val;
		_valuEnt = valuEnt;
	}

	void copyBase()
	{
		auto base = File(base, "r");
		string line;
		while((line = base.readln()) != null)
		{
			_file.write(line);
		}
	}

	void coqProof()
	{
		copyBase();
		suite();
	}
	void suite()
	{
		_file.writeln();
		_file.writeln("Definition F := " ~ fToString(_formule) ~ ".");

		string listAt;
		string parFer;
		foreach(i;1.._varToAtome.length)
		{
			listAt ~= "NVA " ~ to!string(to!int(_varToAtome[i].fils(0).fils(0).etiquette)-1) ~ " " ~ to!string(to!int(_varToAtome[i].fils(1).fils(0).etiquette)-1) ~ " (";
			parFer ~= ")";
		}
		_file.writeln("Definition listAt := " ~ listAt ~ "EA" ~ parFer ~ ".");

		if(_sat)
		{
			string V;
			parFer = "";
			foreach(i;1.._formule.entierMax() + 1)
			{
				if(_valu.getVal(i) == Vrai)
					V ~= "Vrai ";
				else
					V ~= "Faux ";
				V ~= " (";
				parFer ~= ")";
			}
			_file.writeln("Definition V := " ~ V ~ "EV" ~ parFer ~ ".");

			string ve;
			parFer = "";
			foreach(i;1.._valuEnt.length)
			{
				ve ~= "NV " ~ to!string(_valuEnt[i]) ~ " (";
				parFer ~= ")";
			}
			_file.writeln("Definition VE := " ~ ve ~ "EVE" ~ parFer ~ ".");
			_file.writeln();

			_file.writeln("Lemma satF : sat F listAt.");
			proofTrue();
			_file.writeln("Qed.");
			_file.writeln();

			_file.writeln("Check satF.");
		}
		else
		{
			_file.writeln();
			_file.writeln("Lemma nSatF : ~(sat F listAt).");
			proofFalse();
			_file.writeln("Qed.");
			_file.writeln();

			_file.writeln("Check nSatF.");
		}
	}

	void proofTrue()
	{
		_file.writeln("exists V.");
		_file.writeln("split.");
		_file.writeln("simpl.");
		_file.writeln("trivial.");
		_file.writeln("split.");
		_file.writeln("simpl.");
		_file.writeln("firstorder.");
		_file.writeln("exists VE.");
		_file.writeln("simpl.");
		_file.writeln("firstorder.");
		//pareil firstorder rend ça inutile.
		//foreach(i;1.._varToAtome.length)
		//{
		//	_file.writeln("split.");
		//	_file.writeln("auto.");
		//}
		//_file.writeln("trivial.");
	}
	deprecated void proofDPLL(Arbre a, bool needFalse = false, int us = 0)
	{
		//avant de découvrir cette merveilleuse tactique qu'est firstorder, voilà ce que j'avais fait. Pour rien. :'('
		if(a.etiquette == "/\\\\")
		{
			if(!needFalse)
			{
				_file.writeln("split.");
				proofDPLL(a.fils(0), needFalse, us);
				proofDPLL(a.fils(1), needFalse, us);
			}
			else
			{
				if(!satisf(a.fils(0), _valu.variable()))
				{
					_file.writeln("destruct H as (H & us" ~ to!string(us) ~ ").");
					proofDPLL(a.fils(0), needFalse, us + 1);
				}
				else
				{
					_file.writeln("destruct H as (us" ~ to!string(us) ~ " && H).");
					proofDPLL(a.fils(1), needFalse, us +1);
				}
			}

		}
		else if(a.etiquette == "\\\\/")
		{
			if(!needFalse)
			{
				if(satisf(a.fils(0), _valu.variable()))
				{
					_file.writeln("left.");
					proofDPLL(a.fils(0), needFalse, us);
				}
				else
				{
					_file.writeln("right.");
					proofDPLL(a.fils(1), needFalse, us);
				}
			}
			else
			{
				_file.writeln("destruct H.");
				proofDPLL(a.fils(0), needFalse,us);
				proofDPLL(a.fils(1), needFalse,us);
			}
		}
		else if(a.etiquette == "~")
		{
			if(!needFalse)
			{
				_file.writeln("intro H.");
				proofDPLL(a.fils(0), !needFalse, us);
			}
			else
			{
				_file.writeln("apply H.");
				proofDPLL(a.fils(0), !needFalse, us);
			}
		}
		else
		{
			_file.writeln("trivial.");
		}
	}

	void proofFalse()
	{
		_file.writeln("intro.");
		_file.writeln("destruct H as (V & H).");
		_file.writeln("destruct H as (taille & H0).");
		_file.writeln("destruct H0 as (val&valent).");
		_file.writeln("destruct valent as (VE & valent).");
		valu_t[] V;
		V.length = _varToAtome.length;
		V[] = Inconnu;
		int i = 1;
		while(i != 0)
		{
			if(i <= V.length - 1)
			{
				if(V[i] == Inconnu)
				{
					_file.writeln("destruct V.");
					_file.writeln("simpl in taille.");
					_file.writeln("inversion taille.");
					V[i] = Faux;
					i++;
				}
				else if(V[i] == Faux)
				{
					V[i] = Vrai;
					i++;
				}
				else
				{
					V[i] = Inconnu;
					i--;
				}
			}
			else
			{
				_file.writeln("destruct V.");
				//le cas qui nous interesse : la valuation a la bonne taille
				if(satisf(_formule, V))
				{
					_file.writeln("simpl in valent.");
					GrapheCoq g = new GrapheCoq(V.length);
					UnionFind u = new UnionFind(V.length);
					foreach(k;1.._varToAtome.length)
					{
						var_t v1 = to!var_t(_varToAtome[k].fils(0).fils(0).etiquette);
						var_t v2 = to!var_t(_varToAtome[k].fils(1).fils(0).etiquette);
						if(V[k] == Vrai)
						{
							u.unionF(v1, v2);
							g.addArrete(v1,v2);
						}
						_file.writeln("destruct valent as (H" ~ to!string(v1-1) ~"e" ~to!string(v2-1) ~ "&valent).");
					}
					bool proofDone = false;
					foreach(k;1.._varToAtome.length)
					{
						var_t v1 = to!var_t(_varToAtome[k].fils(0).fils(0).etiquette);
						var_t v2 = to!var_t(_varToAtome[k].fils(1).fils(0).etiquette);

						if(V[k] ==  Faux && u.find(v1)==u.find(v2))
						{
							_file.writeln("apply H" ~ to!string(v1-1) ~ "e" ~ to!string(v2-1) ~ ".");
							var_t[] chemin = g.chemin(v1, v2);
							foreach(l;1..chemin.length)
							{
								_file.writeln("transitivity (getValEnt VE " ~ to!string(chemin[l]-1) ~ ").");
								_file.writeln("auto.");
							}
							_file.writeln("auto.");
							break;
						}
					}
				}
				else
				{
					_file.writeln("simpl in val.");
					_file.writeln("firstorder.");
				}

				//la valuation est trop grande
				fauxValTropGrand(V.length); //cas faux
				fauxValTropGrand(V.length); //cas vrai

				i--;
			}
		}

	}

	void fauxValTropGrand(ulong tailleV)
	{
		_file.writeln("simpl in taille.");
		_file.writeln("assert (" ~ to!string(tailleV) ~  "<=" ~ to!string(tailleV-1) ~") as tropGrand.");
		_file.write("transitivity (");
		foreach(useless;0..tailleV)
		{
			_file.write("S (");
		}
		_file.write("length V)");
		foreach(useless;0..tailleV)
		{
			_file.write(")");
		}
		_file.writeln(".");
		foreach(useless;0..tailleV)
		{
			_file.writeln("apply le_n_S.");
		}
		_file.writeln("apply natPositif.");
		_file.writeln("rewrite taille.");
		_file.writeln("constructor.");
		_file.writeln("assert (~"~to!string(tailleV) ~ "<=" ~ to!string(tailleV-1) ~") as tropGrandFaux.");
		_file.writeln("auto with arith.");
		_file.writeln("apply tropGrandFaux.");
		_file.writeln("apply tropGrand.");
	}

	string fToString(Arbre a)
	{
		if(a.etiquette == "/\\\\")
		{
			return "(" ~ fToString(a.fils(0)) ~"/_\\" ~ fToString(a.fils(1)) ~ ")";
		}
		else if(a.etiquette == "\\\\/")
		{
			return "(" ~ fToString(a.fils(0)) ~"\\_/" ~ fToString(a.fils(1)) ~ ")";
		}
		else if(a.etiquette == "~")
		{
			return "(~_" ~ fToString(a.fils(0)) ~ ")";
		}
		else
		{
			return "#" ~ (to!string(to!int(a.etiquette) - 1));
		}
	}

	bool satisf(Arbre a, valu_t[] valu)
	{
		if(a.etiquette == "/\\\\")
		{
			return satisf(a.fils(0), valu) && satisf(a.fils(1), valu);
		}
		else if(a.etiquette == "\\\\/")
		{
			return satisf(a.fils(0), valu) || satisf(a.fils(1), valu);
		}
		else if(a.etiquette == "~")
		{
			return !satisf(a.fils(0), valu);
		}
		else
		{
			return valu[to!ulong(a.etiquette)] == Vrai;
		}
	}
}