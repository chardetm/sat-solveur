module coq.graphe;

import types.basic_types;

import std.stdio;

class GrapheCoq
{
protected:
	bool[][] _matrAdj;
	bool[] _visited;

public:
	this(ulong taille)
	{
		_matrAdj.length = taille+1;
		_visited.length = taille+1;
		foreach(i;1..taille+1)
		{
			_matrAdj[i].length = taille+1;
			_visited[i] = false;
			foreach(j;1..taille+1)
			{
				_matrAdj[i][j] = false;
			}
		}
	}

	void addArrete(var_t i, var_t j)
	{
		_matrAdj[i][j] = true;
		_matrAdj[j][i] = true;
	}

	var_t[] chemin(var_t i, var_t j)
	{
		var_t[] rep;
		if(i==j)
		{
			rep ~= j;
			return rep;
		}
		else if(!_visited[i])
		{
			_visited[i] = true;
			foreach(s;1.._matrAdj.length)
			{
				if(_matrAdj[i][s])
				{
					var_t[] temp = chemin(s,j);
					if(temp.length > 0)
					{
						rep = [i]~temp;
					}
				}
			}
			return rep;
		}
		else
			return rep;
	}
}