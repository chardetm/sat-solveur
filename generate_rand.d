﻿module random_tests_d;

import std.stdio, std.conv, std.string, std.process, std.exception;

void writeUsage (ref string prog_name) {
	writeln("Usage: ", prog_name, " k_max num_var_max num_cl_max num_sample");
	writeln(" - k: number of literals per clause (2 to k_max)");
	writeln(" - num_var: number of variables (k to num_var_max)");
	writeln(" - num_cl: number of clauses (1 to num_cl_max)");
	writeln(" - num_sample: number of generated files per configuration");
}

int main (string[] args) {
	if (args.length != 5) {
		writeUsage(args[0]);
		return 1;
	}
	ulong k_max, num_var_max, num_cl_max, num_sample;
	try {
		k_max = to!ulong(args[1]);
		num_var_max = to!ulong(args[2]);
		num_cl_max = to!ulong(args[3]);
		num_sample = to!ulong(args[4]);
	} catch (Exception e) {
		writeUsage(args[0]);
		return 1;
	}
	
	if (k_max>num_cl_max) {
		writeln("Error: num_cl_max must be greater than or equal to k_max!");
		return 2;
	}

	executeShell("mkdir random_tests 2> /dev/null");

	for (ulong k=2; k<=k_max; ++k) {
		for (ulong numvars=k; numvars<=num_var_max; ++numvars) {
			for (ulong numclauses=1; numclauses<=num_cl_max; ++numclauses) {
				for (ulong numsample=1; numsample <= num_sample; ++numsample) {
					immutable string command = "./genksat "~to!string(k)~" "~to!string(numvars)~" "~to!string(numclauses)~" > random_tests/rand-k"~to!string(k)~"-nv"~to!string(numvars)~"-nc"~to!string(numclauses)~"-"~to!string(numsample)~".cnf";
					writeln("k:", k, " v:", numvars, " c:", numclauses, " s:", numsample);
					try {
						auto ret = executeShell(command);
						if (ret.status != 0) {
							writeln("Error: returned non-zero for command:");
							writeln(" ", command);
							return 1;
						}
					} catch (Exception e) {
						writeln("Exception for command:");
						writeln(" ", command);
						writeln(e);
					}
				}
			}
		}
	}

	return 0;
}