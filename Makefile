DEPENDANCE= Coq/interface_tsei.d types/union_find.d Coq/graphe_coq.d Coq/interface_ega.d types/arbre.d resol.d input/bison_parsing.d types/instance.d types/basic_types.d types/clause.d types/graphe.d types/valuation.d types/grapheCNF.d user_io/interactive_mode.d user_io/arguments_handler.d
DEPENDANCE+= types/entier.d types/grapheDif.d solveurs/sol_dif.d solveurs/sol_con.d types/termeCon.d solveurs/solveur.d solveurs/sol_ega.d

DEPDPLL= algorithms/dpll.d algorithms/dpll_theorie.d
DEPWL= algorithms/watched_literals_theorie.d algorithms/watched_literals.d types/clause_WL.d

FOBJPARSE= input/parserEGA/ega.tab.o input/parserEGA/lex.ega.o input/parserEGA/parsingEGA.d
FOBJPARSE+= input/parserDIF/dif.tab.o input/parserDIF/lex.dif.o input/parserDIF/parsingDIF.d
FOBJPARSE+= input/parserCON/con.tab.o input/parserCON/lex.con.o input/parserCON/parsingCON.d
FOBJPARSE+= input/parserCNF/cnf.tab.o input/parserCNF/lex.cnf.o input/parserCNF/parsingCNF.d
FOBJPARSE+= input/parserTSEI/tsei.tab.o input/parserTSEI/lex.tsei.o input/parserTSEI/parsingTSEI.d
FOBJPARSE+= input/parserCOL/col.tab.o input/parserCOL/lex.col.o input/parserCOL/parsingCOL.d

DEPGDC=compiler_fixes/string.d

OPTGDC= -Wall -Wdeprecated
OPTDMD= -w -wi

OPTARGSGDC= $(OPTGDC) -O3 -frelease -fno-debug
OPTARGSDMD= $(OPTDMD) -O -release -inline -boundscheck=off -profile

DEBUGARGSGDC= $(OPTGDC) -g
DEBUGARGSDMD= $(OPTDMD) -gc -profile

OUTPUT=resol
OUTPUTWL=resol-wl


all: gdc gdc_WL test test_dir genksat generate_rand
noopt: gdc-noopt gdc_WL-noopt test test_dir genksat generate_rand

dmd_all: dmd dmd_WL dmd_test dmd_test_dir dmd_genksat dmd_generate_rand



gdc: $(DEPENDANCE) $(DEPGDC) $(DEPDPLL) parsers
	@gdc $(OPTARGSGDC) $(DEPENDANCE) $(DEPGDC) $(DEPDPLL) $(FOBJPARSE) -o $(OUTPUT) -fversion=DPLL -ll
gdc-noopt: $(DEPENDANCE) $(DEPGDC) $(DEPDPLL) parsers
	@gdc $(DEBUGARGSGDC) $(DEPENDANCE) $(DEPGDC) $(DEPDPLL) $(FOBJPARSE) -o $(OUTPUT) -fversion=DPLL -ll



gdcd: $(DEPENDANCE) $(DEPGDC) $(DEPDPLL) parsers
	@gdc $(DEBUGARGSGDC) $(DEPENDANCE) $(DEPGDC) $(DEPDPLL) $(FOBJPARSE) -o $(OUTPUT) -fdebug=1 -fversion=DPLL -ll
gdcdd: $(DEPENDANCE) $(DEPGDC) $(DEPDPLL) parsers
	@gdc $(DEBUGARGSGDC) $(DEPENDANCE) $(DEPGDC) $(DEPDPLL) $(FOBJPARSE) -o $(OUTPUT) -fdebug=2 -fversion=DPLL -ll

gdc_WL: $(DEPENDANCE) $(DEPGDC) $(DEPWL) parsers
	@gdc $(OPTARGSGDC) $(DEPENDANCE) $(DEPGDC) $(DEPWL) $(FOBJPARSE) -o $(OUTPUTWL) -fversion=watchedLiterals -ll
gdc_WL-noopt: $(DEPENDANCE) $(DEPGDC) $(DEPWL) parsers
	@gdc $(DEBUGARGSGDC) $(DEPENDANCE) $(DEPGDC) $(DEPWL) $(FOBJPARSE) -o $(OUTPUTWL) -fversion=watchedLiterals -ll
gdc_WLd: $(DEPENDANCE) $(DEPGDC) $(DEPWL) parsers
	@gdc $(DEBUGARGSGDC) $(DEPENDANCE) $(DEPGDC) $(DEPWL) $(FOBJPARSE) -o $(OUTPUTWL) -fdebug=1 -fversion=watchedLiterals -ll


dmd: $(DEPENDANCE) $(DEPDPLL) parsers
	@dmd $(OPTARGSDMD) $(DEPENDANCE) $(DEPDPLL) $(FOBJPARSE) -of$(OUTPUT) -version=DPLL -L-ll
dmdd: $(DEPENDANCE) $(DEPDPLL) parsers
	@dmd $(DEBUGARGSDMD) $(DEPENDANCE) $(DEPDPLL) $(FOBJPARSE) -of$(OUTPUT) -debug -version=DPLL -L-ll

dmd_WL: $(DEPENDANCE) $(DEPDMD) parsers
	@dmd $(OPTARGSDMD) $(DEPENDANCE) $(DEPWL) $(FOBJPARSE) -of$(OUTPUTWL) -version=watchedLiterals -L-ll
dmd_WLd: $(DEPENDANCE) $(DEPDMD) parsers
	@dmd $(DEBUGARGSDMD) $(DEPENDANCE) $(DEPWL) $(FOBJPARSE) -of$(OUTPUTWL) -debug -version=watchedLiterals -L-ll
dmd_WLdd: $(DEPENDANCE) $(DEPDMD) parsers
	@dmd $(DEBUGARGSDMD) $(DEPENDANCE) $(DEPWL) $(FOBJPARSE) -of$(OUTPUTWL) -debug=2 -version=watchedLiterals -L-ll

	
test: test.d
	@gdc test.d -g -o test
	
dmd_test: test.d
	@dmd test.d -gc -oftest

	
test_dir: test_dir.d
	@gdc test_dir.d -g -o test_dir
	
dmd_test_dir: test_dir.d
	@dmd test_dir.d -gc -oftest_dir


genksat: genksat.d
	@gdc $(OPTARGSGDC) genksat.d -o genksat

dmd_genksat: genksat.d
	@dmd $(OPTARGSDMD) genksat.d -ofgenksat


generate_rand: generate_rand.d
	@gdc $(OPTARGSGDC) generate_rand.d -o generate_rand

dmd_generate_rand: generate_rand.d
	@dmd $(OPTARGSDMD) generate_rand.d -ofgenerate_rand
	
	
parsers:
	@cd input ; make -s

clean:
	@cd input/ ; make -s clean
	@rm resol resol-wl test test_dir generate_rand genksat resol.o resol-wl.o test.o test_dir.o generate_rand.o genksat.o 2>/dev/null; true
	
.PHONY: clean input/c_flexbison/cnf.tab.o input/c_flexbison/lex.yy.o
