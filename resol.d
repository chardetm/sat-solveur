﻿module main;

import std.stdio, std.conv, std.string, std.exception, std.file;
import types.instance, types.valuation, input.bison_parsing, types.grapheCNF, types.graphe, user_io.arguments_handler;
import types.arbre, solveurs.sol_ega, solveurs.sol_con, solveurs.sol_dif, coq.interface_tsei;

enum SYNTAX_ERROR = 1, FILE_ERROR = 2, PARSING_ERROR = 3, NOT_HANDLED = 4;

version(watchedLiterals)
{
	import algorithms.watched_literals;
}
version(DPLL)
{
	import algorithms.dpll;
}

void printSyntaxError(string arg0) {
	writeln("Wrong syntax. Usage: ", arg0, " [ -cl | -cl-interact | -color k | -explainunsat | -tseitin | -rand | -moms | -dlis | -vsids | -coq ] input");
}

int main(string[] args) {
	try {
		mode_t choix;
		bool coq;
		ulong nbColors;
		format_t format;
		pari_t choixPari;

		try {
			choix = handleArguments(args, nbColors, format, choixPari, coq);
		} catch (SyntaxError e) {
			printSyntaxError(args[0]);
			return SYNTAX_ERROR;
		}

		File f;

		try {
			if (!(args[1].isFile)) {
				writeln("Error: ", args[1], " is not a file!");
				return FILE_ERROR;
			}
			f.open(args[1], "r");
		} catch (FileException e) {
			writeln("Error: ", args[1], " does not exist or cannot be read!");
			return FILE_ERROR;
		} catch (ErrnoException e) {
			writeln("Error: ", args[1], " does not exist or cannot be read!");
			return FILE_ERROR;
		} catch (Exception e) {
			writeln("Error: ", args[1], " does not exist or cannot be read!");
			return FILE_ERROR;
		}

		if(choix != mode_t.COLOR && format == format_t.CNF)
		{
			version(watchedLiterals)
			{
				InstanceWL inst = new InstanceWL(bisonParsing(f, choix, choixPari));
			}
			version(DPLL)
			{
				InstanceDPLL inst = new InstanceDPLL(bisonParsing(f, choix, choixPari));
			}
			try
			{
				inst.algo();
				writeln("s SATISFIABLE");
				writeln(inst.val);
				debug
				{
					uint count=0;
					for(int i=1; i < inst.val.length; i++)
					{
						if(inst.val.getVal(i) == Inconnu)
							++count;
					}
					if (count>0) {
						writeln("Debug warning: Valuation pas finie, ", count, " variables sur ", inst.val.nbVariables , " n'ont pas de valeur !");
					}
					if (inst.isTrue) {
						writeln("Debug: La valuation satisfait le problème.");
					} else {
						writeln("Debug: La valuation ne satisfait pas le problème.");
					}
				}
				version(DPLL)
				{
					if(coq)
					{
						InterfaceTsei inter = new InterfaceTsei(inst.toFormule(), inst._val, true);
						inter.coqProof();
					}
				}
			}
			catch(NotSolvable e)
			{
				version(DPLL)
				{
					if(coq)
					{
						InterfaceTsei inter = new InterfaceTsei(inst.toFormule(), inst._val, false);
						inter.coqProof();
					}
				}
				throw e;
			}
		}
		else if(choix != mode_t.COLOR)
		{
			string s = f.readln();
			f.seek(0);//on remet le curseur au début du fichier.
			if(s[0..3] == "ega")
				format = format_t.EGA;
			else if(s[0..3] == "dif")
				format = format_t.DIF;
			else if(s[0..3] == "con")
				format = format_t.CON;
			else if(s[0..4] == "tsei")
				format = format_t.OTHER;
			else
				throw new ParsingException("Format not detected.");
			if (format == format_t.EGA)
			{
				Arbre a = parsingEga(f);
				SolveurEga sol = new SolveurEga(a, choixPari, choix, coq);
				sol.solve();
			}
			else if (format == format_t.CON)
			{
				Arbre a = parsingCon(f);
				SolveurCon sol = new SolveurCon(a, choixPari, choix);
				sol.solve();
			}
			else if(format == format_t.DIF)
			{
				Arbre a = parsingDif(f);
				SolveurDif sol = new SolveurDif(a, choixPari, choix);
				sol.solve;
			}
			else if(format == format_t.OTHER)
			{
				Arbre a = parsingTsei(f);
				Instance ins = a.transfoTseitin(choixPari, choix);

				version(DPLL)
				{
					InstanceDPLL inst = new InstanceDPLL(ins);
				}
				version(watchedLiterals)
				{
					InstanceWL inst = new InstanceWL(ins);
				}

				try
				{
					inst.algo();
					writeln("s SATISFIABLE");
					writeln(inst.val);
					debug {
						uint count=0;
						for (int i = 1; i < inst.val.length; i++) {
							if(inst.val.getVal(i) == Inconnu)
								++count;
						}
						if (count>0) {
							writeln("Debug warning: Valuation pas finie, ", count, " variables sur ", inst.val.nbVariables , " n'ont pas de valeur !");
						}
						if (inst.isTrue) {
							writeln("Debug: La valuation satisfait le problème.");
						} else {
							writeln("Debug: La valuation ne satisfait pas le problème.");
						}
					}
					if(coq)
					{
						InterfaceTsei inter = new InterfaceTsei(a, inst._val,true);
						inter.coqProof();
					}
				}
				catch(NotSolvable e)
				{
					if(coq)
					{
						InterfaceTsei inter = new InterfaceTsei(a, inst._val,false);
						inter.coqProof();
					}
					throw e;
				}
			}
		}
		else
		{
			Graphe g;
			g = parsingCol(f);

			version(watchedLiterals)
			{
				InstanceWL inst = new InstanceWL(g.toInstance(nbColors, choixPari, choix));
			}
			version(DPLL)
			{
				InstanceDPLL inst = new InstanceDPLL (g.toInstance(nbColors, choixPari, choix));
			}
			inst.algo();
			g.coloriageGraph(inst.val, nbColors);
			g.toPDF();
			debug {
				uint count=0;
				for(int i = 1; i < inst.val.length; i++)
				{
					if(inst.val.getVal(i) == Inconnu)
						++count;
				}
				if (count>0) {
					writeln("Debug warning: Valuation pas finie, ", count, " variables sur ", inst.val.nbVariables , " n'ont pas de valeur !");
				}
				if(inst.isTrue)
				{
					writeln("Debug: La valuation satisfait le problème.");
				} else {
					writeln("Debug: La valuation ne satisfait pas le problème.");
				}
			}
		}
	} catch (ParsingException e) {
		writeln("Mauvais format de fichier DIMACS");
		debug {
			writeln(e);
		}
		return PARSING_ERROR;
	} catch (NotSolvable e) {
		writeln("s UNSATISFIABLE");
		debug {
			writeln(e);
		}
	} catch (Exception e) {
		writeln("Exception inatendue :");
		writeln(e);
	}
	return 0;
}

