module algorithms.watched_literals;

import std.stdio, std.conv, std.exception, std.math;
import types.basic_types, types.clause_WL, types.valuation, types.instance, input.bison_parsing, types.grapheCNF;
import types.clause, types.arbre;
import user_io.interactive_mode;

class InstanceWL
{
	public Valuation _val;
	protected ClauseWL[] _clauses;
	protected ulong _C;
	protected mode_t _mode;
	
	
	this(ref File f, mode_t choix, pari_t pariChoix)
	{
		this(bisonParsing(f, choix, pariChoix));
		_mode = choix;
	}

	this(mode_t choix, pari_t pariChoix = pari_t.NORMAL)
	{
		_mode = choix;
		_val = new Valuation(pariChoix);
	}

	this (Instance f)
	{
		//transforme une instance normale en une instance WL
		_val = f.val;
		_mode = f.mode;
		_clauses.length = f.clauseLength;
		for(int i = 0; i <_clauses.length	;++i)
		{
			_clauses[i] = new ClauseWL(f.getClause(i));
		}
		_C = f.clauseLength; 
	}

	void init(var_t V, uint C)
	{
		this._C = _C;
		_val.init(V);
	}

	void addClause(ClauseWL c)
	{
		_clauses ~= c;
	}
	void setVal(Valuation val)
	{
		_val = val;
	}

	@property override string toString()
	{
		string str = _val.toString;
		str ~= "\n";
		str ~= to!string(_clauses.length) ~ "\n";
		foreach(e;_clauses)
		{
			str ~= e.toString;
		}
		return str;
	}

	@property bool fairePari()
	{
		debug(2)
		{
			writeln("Debug : Pari WL");
		}
		Clause[] c = cast(Clause[])_clauses;
		return _val.fairePari(c);
	}

	@property bool isTrue()
	{
		bool res = true;
		foreach(e;_clauses)
		{
			res = e.isSatisfied;
			if(!res)
				break;
		}
		return res;
	}

	void algo()
	{
		initAlgo();
		while(fairePari)
		{
			while(!update()){}
		}
	}

	void initAlgo()
	{
		//pretraitement des données
		foreach(e; _clauses)
		{
			if(e.isTautology)
			{
				//Rien à faire, la clause est désactivé.
			}
			else if(e.isMonome && _val.getVal(abs(e.getLiteral(0))) == Inconnu)
			{
				_val.modif(abs(e.getLiteral(0)), e.getLiteral(0) > 0, e);
			}
		}
		while(!update()){}
		//pretraitemente terminé
	}

	bool update()//retourne faux s'il y a besoin d'un autre update à la fin (par exemple : à cause d'une déduction)
	{
		debug(2) {
			writeln(_val);
			writeln(_val.etage);
			readln();
		}
		bool res = true;
		foreach(e; _clauses)
		{
			res = e.updateWL();
			//on remarque que si res est faux, _lit1 et/ou _lit2 est faux.
			if(!res)
			{
				if(!e.deductionWL())
				{
					//besoin d'un backtracking
					if(_mode == mode_t.NORMAL)
					{
						//mode sans apprentissage de clause
						//backtracking chronologique.
						_val.backtrack();
					}
					else
					{
						//apprentissage de clause
						//backtracking intelligent
						imChoice_t choix; //choix de l'utilisateur (g, t ou c)
						if(_mode == mode_t.CL_INTERAC)
						{
							choix = getUserChoice();
							if(choix == imChoice_t.TERMINATE)
							{
								_mode = mode_t.CL; //on enleve le mode interactif
							}
						}
						//on crée la clause à rajouter	
						ClauseWL c = e;
						var_t[] newVar = c.getNew;
						var_t pivot;
						while(newVar.length > 1) //tant qu'il y a deux variables mis à jour à cet etage
						{
							pivot = Clause.getNonPari(newVar); //on en prend une différent du dernier pari
							debug(2){
								writeln(c);
								writeln(_val.getRaisonModif(pivot));
								writeln(pivot);
							}
							c = ClauseWL.resolution(c, new ClauseWL(_val.getRaisonModif(pivot)), pivot); //on fait la résolution
							debug(2) {
								writeln(c);
								readln();
							}
							newVar = c.getNew; //et on itere
						}
						pivot = newVar[0];
						if(choix == imChoice_t.GRAPH){
							//on fait l'arbre de conflit
							GrapheCNF g = new GrapheCNF(_val.nbVariables);
							g.setValu(_val);
							g.create(e);
							g.toPDF(c,pivot);
						}
						addClause(c); //on ajoute la clause au problème
						c.augmenteScore();
						long nouvEtage = _val.backtrack(pivot,c); //et on fait notre backtracking
						if(c.isMonome)
						{
							//si c est un monome, on peut directement en déduire une valeur
							bool resModif = _val.modif(abs(c.getLiteral(0)), c.getLiteral(0) > 0, c);
							if(!resModif)
							{
								if(_mode == mode_t.EXPLAIN)
									_val.creerContradProof(c, abs(c.getLiteral(0)));
								throw new NotSolvable("Monome contradictoire");
							}
							debug(2) {
								writeln(c, " est un monomone.");
								readln();
							}
						}
						if(nouvEtage == -1)
						{
							writeln("On ne savait pas si ce cas pouvait arriver.");
							writeln("Si vous voyez ce message, pouvez-vous nous envoyer un mail avec le fichier cnf utilisé ?");
							writeln("Merci !");
							throw new NotSolvable("Envoyer un mail !");
						}
						if(_mode == mode_t.CL_INTERAC)
						{
							writeln(c);
							writeln(nouvEtage);
						}
					}
					return update();
				}
				break;
			}
		}
		return res;
	}


	@property Valuation val () {
		return _val;
	}

	Arbre toFormule()
	 {
	 	Arbre a;
	 	if(_clauses.length == 0)
	 		return a;
	 	a = _clauses[0].toArbre();
	 	if(a is null)
	 		return null;
	 	foreach(i;1.._clauses.length)
	 	{
	 		Arbre clause = _clauses[i].toArbre();
	 		if(clause is null)
	 			return null;
	 		Arbre te = new Arbre("/\\\\");
	 		te.addFils(a);
	 		te.addFils(clause);
	 		a = te;
	 	}

	 	return a;
	 }
}
