﻿module algorithms.dpll_theorie;

import algorithms.dpll, solveurs.solveur;
import std.stdio, std.conv, std.exception, std.math;
import types.instance, types.basic_types, types.clause, user_io.interactive_mode, types.grapheCNF;

final class InstanceDPLLTh : InstanceDPLL {

	protected Solveur _solv;

	deprecated this() {
		// Nécessaire pour la compilation (instance temporaire)
		super(mode_t.NORMAL);
	}
	
	this (mode_t mode, Solveur solv, pari_t choixPari) {
		super(mode, choixPari);
		_solv = solv;
	}
	
	this (Instance i, Solveur solv) { // Copy constructor
		super(i);
		_solv = solv;
	}
	
	this (File f, mode_t mode, Solveur solv) {
		super(f, mode);
		_solv = solv;
	}
	
	override protected void backtrack() {
		debug(2) {
			writeln("Debug: Backtrack DPLL(T)");
		}
		
		if (_backtrackLimit.length == 0) { // Si pas de limite, on vide tout
			foreach_reverse (c; _disabledClauses) {
				_clauses ~= c;
			}
			_disabledClauses.length = 0;
			
		} else { // Sinon on vide jusqu'à la limite
			assert(_disabledClauses.length >= _backtrackLimit[$-1]);
			if (_disabledClauses.length > 0) { // To avoid unsigned -1 problems
				for (size_t i = _disabledClauses.length-1; i >= _backtrackLimit[$-1]; --i) {
					_clauses ~= _disabledClauses[i];
					if (i == 0) { // To avoid unsigned -1 problems
						break;
					}
				}
			}
			_disabledClauses.length = _backtrackLimit[$-1];
			--_backtrackLimit.length;
		}
		
		_val.backtrack(); // Backtracking au niveau de la valuation
		_solv.backtrack(); // Backtracking au niveau du solveur de théorie
		if (!_solv.markPari()) {
			debug(2) {
				writeln("Debug: backtrack récursif DPLL(T)");
			}
			backtrack();
		}
	}
	
	override protected ulong backtrack(var_t pivot, Clause cl) { // Version CL
		debug(2) {
			writeln("Debug: Backtrack");
		}
		
		void backtrackUnEtage() { // Fonction auxiliaire, pour ne remonter que d'un étage
			if (_backtrackLimit.length == 0) { // Si pas de limite, on vide tout
				foreach_reverse (c; _disabledClauses) {
					_clauses ~= c;
				}
				_disabledClauses.length = 0;
				
			} else { // Sinon on vide jusqu'à la limite
				assert(_disabledClauses.length >= _backtrackLimit[$-1]);
				if (_disabledClauses.length > 0) { // To avoid unsigned -1 problems
					for (size_t i = _disabledClauses.length-1; i >= _backtrackLimit[$-1]; --i) {
						_clauses ~= _disabledClauses[i];
						if (i == 0) { // To avoid unsigned -1 problems
							break;
						}
					}
				}
				_disabledClauses.length = _backtrackLimit[$-1];
				--_backtrackLimit.length;
			}
		}
		
		long etageCourant = _val.etage;
		long nouvelEtage = _val.backtrack(pivot,cl);
		
		if (nouvelEtage != -1) { // Si on n'est pas à la fin
			for (long i=nouvelEtage; i<etageCourant; ++i) { // On backtrack autant de fois qu'il faut
				backtrackUnEtage();
			}
			_solv.backtrackToLevel(nouvelEtage);
		} else { // Sinon, pas de solution
			
			throw new NotSolvable("Backtrack sur pile pari vide");
		}
		
		return nouvelEtage; // Backtracking au niveau de la valuation
	}
	
	
	override protected bool fairePari() {
		debug(2) {
			writeln("Debug: Fait un pari DPLL(T)");
		}
		_backtrackLimit~=_disabledClauses.length; // On marque la nouvelle limite pour le backtrack
		if (!_val.fairePari(_clauses)) return false; // Et on fait le pari au niveau de la valuation (AVANT de le signaler...)
		if (!_solv.markPari()) { // On signale un pari au solveur
			backtrack();
		}
		return true; 
	}

	bool modifVal (var_t var, bool val, Clause c) {
		return _val.modif(var, val, c) && _solv.markChangeVar();
	}
	
	override protected bool findAffectations () {
		debug(2) {
			writeln("Debug: Find Affectations DPLL(T)");
		}
		
		bool changed = false; // Marqueur pour savoir s'il y a eu un changement (pour savoir si on doit rechercher des affectations)
		
		foreach (ref e; _clauses) { // On parcourt les clauses
			if (e.isEmpty()) {
				changed = true; // Si une clause est vide il faudra faire un backtrack
			}
			
			if (e.isMonome) {
				lit_t l = e.getFirstUnknownLiteral();
				if (modifVal(abs(l), l > 0, e)) { // Si c'est un monome, alors son littéral est forcément vrai
					changed = true; // Si l'affectation s'est bien passée, un changement a eu lieu
				} else {
					backtrack();
					// TODO: Rajouter la récupération d'une explication ("pourquoi ça ne marche pas")
					// _slov.stillSolvableR() != NULL
					return false;
				}
			}
		}
		
		return changed;
	}
	
	override protected void applyPropagation () {
		debug(2) {
			writeln("Debug: Apply Propagation DPLL(T)");
		}
		for (size_t i=0; i<_clauses.length; ++i) {
			if (_clauses[i].isSatisfied) { // On supprime les clauses satisfaites
				_disabledClauses~=_clauses[i];
				_clauses[i] = _clauses[$-1];
				--_clauses.length;
				--i;
				
			} else if (_clauses[i].isEmpty) { // Si on tombe sur la clause vide, alors on doit faire un backtrack
				if (_mode == mode_t.NORMAL) {
					backtrack(); // Besoin de backtrack
				} else {
					// Apprentissage de clause
					// Backtracking intelligent
					imChoice_t choix; // Choix de l'utilisateur (g, t ou c)
					if (_mode == mode_t.CL_INTERAC) {
						choix = getUserChoice();
						if (choix == imChoice_t.TERMINATE) {
							_mode = mode_t.CL; // On enlève le mode interactif
						}
					}
					// On crée la clause à rajouter	
					Clause c = _clauses[i];
					var_t[] newVar = c.getNew;
					var_t pivot;
					while (newVar.length > 1) { // Tant qu'il y a deux variables mises à jour à cet etage
						pivot = Clause.getNonPari(newVar); // On en prend une différente du dernier pari
						debug(2) {
							writeln(c);
							writeln(_val.getRaisonModif(pivot));
							writeln(pivot);
						}
						c = Clause.resolution(c, _val.getRaisonModif(pivot), pivot); // On fait la résolution /*_val.getRaisonModif(pivot).activeClause*/ ?
						debug(2) {
							writeln(c);
							readln();
						}
						newVar = c.getNew; // Et on itere
					}
					pivot = newVar[0];
					if (choix == imChoice_t.GRAPH) {
						// On fait l'arbre de conflit
						GrapheCNF g = new GrapheCNF(_val.nbVariables);
						g.setValu(_val);
						g.create(_clauses[i]);
						g.toDot(c,pivot);
					}
					addClause(c); // On ajoute la clause au problème
					
					ulong nouvEtage = backtrack(pivot,c); // Et on fait notre backtracking
					if (_mode == mode_t.CL_INTERAC) {
						writeln(c);
						writeln(nouvEtage);
					}
				}
			}
		}
	}
	
	override public void algo () {
		
		// Premières vérifications
		firstCheck();
		
		do {
			// Tant qu'on trouve des affectations, on continue
			while (findAffectations()) {
				applyPropagation(); // S'occupe du backtrack si besoin
			}
		} while (fairePari());             // Quand on n'a plus d'affectations trouvées, on fait un nouveau pari si possible, sinon on a fini

		_clauses~=_disabledClauses; // Pour pouvoir tester la solution trouvée en mode debug
		_disabledClauses.length = 0;
	}

	string toCNF()
	{
		string str = "p cnf " ~ to!string(_val.V()) ~ " " ~ to!string(_clauses.length) ~"\n";
		foreach(e;_clauses)
		{
			str ~= e.toString() ~ "\n";
		}
		return str;
	}
	
}
