module algorithms.watched_literals_theorie;

import algorithms.watched_literals, solveurs.solveur;
import std.stdio, std.conv, std.exception, std.math;
import types.basic_types, types.clause_WL, types.valuation, types.instance, input.bison_parsing, types.grapheCNF;
import types.clause, types.arbre;
import user_io.interactive_mode;

final class InstanceWLTheo : InstanceWL
{
protected:
	Solveur _solv;

public:
	this()
	{
		super(mode_t.NORMAL);
	}
	this(Instance i, Solveur solv)
	{
		super(i);
		_solv = solv;
	}
	this (mode_t mode, Solveur solv, pari_t pariChoix = pari_t.NORMAL)
	{
		super(mode, pariChoix);
		_solv = solv;
	}

	void backtrack()
	{
		_val.backtrack();
		_solv.backtrack();
		if(!_solv.markPari())
		{
			backtrack();
		}
	}

	override bool fairePari()
	{
		if(!super.fairePari())
			return false;
		if(!_solv.markPari())
		{
			backtrack();
		}
		return true;
	}

	override bool update()
	{
		debug(2) {
			writeln(_val);
			writeln(_val.etage);
			readln();
		}
		bool res = true;
		foreach(e; _clauses)
		{
			res = e.updateWL();
			//on remarque que si res est faux, _lit1 et/ou _lit2 est faux.
			if(!res)
			{
				if(!e.deductionWL() || !_solv.markChangeVar())
				{
					//besoin d'un backtracking
					if(_mode == mode_t.NORMAL)
					{
						//mode sans apprentissage de clause
						//backtracking chronologique.
						backtrack();
					}
					else
					{
						//apprentissage de clause
						//backtracking intelligent
						imChoice_t choix; //choix de l'utilisateur (g, t ou c)
						if(_mode == mode_t.CL_INTERAC)
						{
							choix = getUserChoice();
							if(choix == imChoice_t.TERMINATE)
							{
								_mode = mode_t.CL; //on enleve le mode interactif
							}
						}
						//on crée la clause à rajouter	
						ClauseWL c = e;
						var_t[] newVar = c.getNew;
						var_t pivot;
						while(newVar.length > 1) //tant qu'il y a deux variables mis à jour à cet etage
						{
							pivot = Clause.getNonPari(newVar); //on en prend une différent du dernier pari
							debug(2){
								writeln(c);
								writeln(_val.getRaisonModif(pivot));
								writeln(pivot);
							}
							c = ClauseWL.resolution(c, new ClauseWL(_val.getRaisonModif(pivot)), pivot); //on fait la résolution
							debug(2) {
								writeln(c);
								readln();
							}
							newVar = c.getNew; //et on itere
						}
						pivot = newVar[0];
						if(choix == imChoice_t.GRAPH){
							//on fait l'arbre de conflit
							GrapheCNF g = new GrapheCNF(_val.nbVariables);
							g.setValu(_val);
							g.create(e);
							g.toPDF(c,pivot);
						}
						addClause(c); //on ajoute la clause au problème
						c.augmenteScore();
						long nouvEtage = _val.backtrack(pivot,c); //et on fait notre backtracking
						if(c.isMonome)
						{
							//si c est un monome, on peut directement en déduire une valeur
							bool resModif = _val.modif(abs(c.getLiteral(0)), c.getLiteral(0) > 0, c);
							if(!resModif)
							{
								if(_mode == mode_t.EXPLAIN)
									_val.creerContradProof(c, abs(c.getLiteral(0)));
								throw new NotSolvable("Monome contradictoire");
							}
							debug(2) {
								writeln(c, " est un monomone.");
								readln();
							}
						}
						if(nouvEtage == -1)
						{
							writeln("On ne savait pas si ce cas pouvait arriver.");
							writeln("Si vous voyez ce message, pouvez-vous nous envoyer un mail avec le fichier cnf utilisé ?");
							writeln("Merci !");
							throw new NotSolvable("Envoyer un mail !");
						}
						if(_mode == mode_t.CL_INTERAC)
						{
							writeln(c);
							writeln(nouvEtage);
						}
						_solv.backtrackToLevel(nouvEtage);
					}
					return update();
				}
				break;
			}
		}
		return res;
	}
}