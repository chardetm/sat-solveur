﻿module algorithms.dpll;

import std.stdio, std.conv, std.exception, std.math;
import types.instance, types.basic_types, types.clause, user_io.interactive_mode, types.grapheCNF;

class InstanceDPLL : Instance {

	protected Clause[] _disabledClauses; // Clauses désactivées
	protected size_t[] _backtrackLimit; // On se rappelle des tailles de _disabledClauses pour le backtrack

	deprecated this() {
		// Nécessaire pour la compilation (instance temporaire)
		super(mode_t.NORMAL, pari_t.NORMAL);
	}

	this (mode_t mode, pari_t choix = pari_t.NORMAL) {
		super(mode, choix);
	}

	this (Instance i) { // Copy constructor
		super(i);
	}

	this (File f, mode_t mode, pari_t choix = pari_t.NORMAL) {
		super(f, mode, choix);
	}

	protected void backtrack() {
		debug(2) {
			writeln("Debug: Backtrack");
		}

		if (_backtrackLimit.length == 0) { // Si pas de limite, on vide tout
			foreach_reverse (c; _disabledClauses) {
				_clauses ~= c;
			}
			_disabledClauses.length = 0;

		} else { // Sinon on vide jusqu'à la limite
			assert(_disabledClauses.length >= _backtrackLimit[$-1]);
			if (_disabledClauses.length > 0) { // To avoid unsigned -1 problems
				for (size_t i = _disabledClauses.length-1; i >= _backtrackLimit[$-1]; --i) {
					_clauses ~= _disabledClauses[i];
					if (i == 0) { // To avoid unsigned -1 problems
						break;
					}
				}
			}
			_disabledClauses.length = _backtrackLimit[$-1];
			--_backtrackLimit.length;
		}

		_val.backtrack(); // Backtracking au niveau de la valuation
	}
	
	protected ulong backtrack(var_t pivot, Clause cl) { // Version CL
		debug(2) {
			writeln("Debug: Backtrack");
		}

		void backtrackUnEtage() { // Fonction auxiliaire, pour ne remonter que d'un étage
			if (_backtrackLimit.length == 0) { // Si pas de limite, on vide tout
				foreach_reverse (c; _disabledClauses) {
					_clauses ~= c;
				}
				_disabledClauses.length = 0;
				
			} else { // Sinon on vide jusqu'à la limite
				assert(_disabledClauses.length >= _backtrackLimit[$-1]);
				if (_disabledClauses.length > 0) { // To avoid unsigned -1 problems
					for (size_t i = _disabledClauses.length-1; i >= _backtrackLimit[$-1]; --i) {
						_clauses ~= _disabledClauses[i];
						if (i == 0) { // To avoid unsigned -1 problems
							break;
						}
					}
				}
				_disabledClauses.length = _backtrackLimit[$-1];
				--_backtrackLimit.length;
			}
		}

		long etageCourant = _val.etage;
		long nouvelEtage = _val.backtrack(pivot,cl);

		if (nouvelEtage != -1) { // Si on n'est pas à la fin
			for (long i=nouvelEtage; i<etageCourant; ++i) { // On backtrack autant de fois qu'il faut
				backtrackUnEtage();
			}
		} else { // Sinon, pas de solution

			throw new NotSolvable("Backtrack sur pile pari vide");
		}

		return nouvelEtage; // Backtracking au niveau de la valuation
	}


	protected bool fairePari() {
		debug(2) {
			writeln("Debug: Fait un pari");
		}
		_backtrackLimit~=_disabledClauses.length; // On marque la nouvelle limite pour le backtrack
		return _val.fairePari(_clauses); // Et on fait le pari au niveau de la valuation
	}
		
	
	protected void firstCheck () { // Premières vérifications
		debug(2) {
			writeln("Debug: First Check");
		}
		for (size_t i=0; i<_clauses.length; ++i) {
			if (_clauses[i].isTautology) {  // On enlève les tautologies
				debug(2) {
					writeln("Debug: ", _clauses[i], " is a tautology");
				}
				_clauses[i] = _clauses[$-1];
				--_clauses.length;
				--i;
			} else if (_clauses[i].isMonome) { // Si deux monomes sont contradictoires, alors le problème n'a pas de solution
				if(_val.isFalse(_clauses[i].getLiteral(0))) {
					debug(2) {
						writeln("Debug: Two clauses are contradictory: ", _clauses[i]);
					}
					throw new NotSolvable("Deux monomes sont contradictoires...");
				}
			}
		}
	}
	
	protected bool findAffectations () {
		debug(2) {
			writeln("Debug: Find Affectations");
		}

		bool changed = false; // Marqueur pour savoir s'il y a eu un changement (pour savoir si on doit rechercher des affectations)

		enum polarity_t {Inconnu, Vrai, Faux, Oubli} // Type énuméré
		polarity_t[] polarity;
		polarity.length = _val.nbVariables+1;

		if (_mode == mode_t.NORMAL) {
			for (var_t i=1; i<= _val.nbVariables; ++i) { // On initialise le tableau des polarités
				final switch (_val.getVal(i)) {
				case Inconnu:
					polarity[i] = polarity_t.Inconnu;
					break;
				case Vrai, Faux: // Si c'est Vrai ou Faux, on s'en fiche
					polarity[i] = polarity_t.Oubli;
					break;
				}
			}
		}

		foreach (ref e; _clauses) { // On parcourt les clauses
			if (e.isEmpty()) {
				changed = true; // Si une clause est vide il faudra faire un backtrack
			}

			if (e.isMonome) {
				lit_t l = e.getFirstUnknownLiteral();
				_val.modif(abs(l), l > 0, e); // Si c'est un monome, alors son littéral est forcément vrai
				if (_mode == mode_t.NORMAL) {
					polarity[abs(l)] = polarity_t.Oubli; // Sera déjà modifié
				}
				changed = true; // Un changement a eu lieu

			} else if (_mode == mode_t.NORMAL) { // Vérification des polarités débranchée en mode CL
				for (size_t i=1; i<polarity.length; ++i) { // Mise à jour des polarités des variables

					final switch (polarity[i]) {
					case polarity_t.Oubli:
						break;
					case polarity_t.Inconnu:
						if (e.has_literal(i)) {
							polarity[i] = polarity_t.Vrai;
						} else if (e.has_literal(-i)) {
							polarity[i] = polarity_t.Faux;
						}
						break;
					case polarity_t.Vrai:
						if (e.has_literal(-i)) {
							polarity[i] = polarity_t.Oubli; // Plusieurs polarités
						}
						break;
					case polarity_t.Faux:
						if (e.has_literal(i)) {
							polarity[i] = polarity_t.Oubli; // Plusieurs polarités
						}
						break;
					}

				}
			}
		}

		if (_mode == mode_t.NORMAL) { // Vérification des polarités débranchée en mode CL
			// Une seule polarité ?
			for (size_t i=1; i<polarity.length; ++i) {
				
				final switch (polarity[i]) {
				case polarity_t.Oubli, polarity_t.Inconnu:
					break;
				case polarity_t.Vrai: // Si une seule polarité, alors on déduit la valeur
					_val.modif(i, true, null); // Pas de clause responsable, exécuté que sans CL
					changed = true;
					break;
				case polarity_t.Faux:
						_val.modif(i, false, null); // Pas de clause responsable, exécuté que sans CL
					changed = true;
					break;
				}
				
			}
		}

		return changed;
	}

	protected void applyPropagation () {
		debug(2) {
			writeln("Debug: Apply Propagation");
		}
		for (size_t i=0; i<_clauses.length; ++i) {
			if (_clauses[i].isSatisfied) { // On supprime les clauses satisfaites
				_disabledClauses~=_clauses[i];
				_clauses[i] = _clauses[$-1];
				--_clauses.length;
				--i;

			} else if (_clauses[i].isEmpty) { // Si on tombe sur la clause vide, alors on doit faire un backtrack
				if (_mode == mode_t.NORMAL) {
					backtrack(); // Besoin de backtrack
				} else {
					// Apprentissage de clause
					// Backtracking intelligent
					imChoice_t choix; // Choix de l'utilisateur (g, t ou c)
					if (_mode == mode_t.CL_INTERAC) {
						choix = getUserChoice();
						if (choix == imChoice_t.TERMINATE) {
							_mode = mode_t.CL; // On enlève le mode interactif
						}
					}
					// On crée la clause à rajouter	
					Clause c = _clauses[i];
					var_t[] newVar = c.getNew;
					var_t pivot;
					while (newVar.length > 1) { // Tant qu'il y a deux variables mises à jour à cet etage
						pivot = Clause.getNonPari(newVar); // On en prend une différente du dernier pari
						debug(2) {
							writeln(c);
							writeln(_val.getRaisonModif(pivot));
							writeln(pivot);
						}
						c = Clause.resolution(c, _val.getRaisonModif(pivot), pivot); // On fait la résolution /*_val.getRaisonModif(pivot).activeClause*/ ?
						debug(2) {
							writeln(c);
							readln();
						}
						newVar = c.getNew; // Et on itere
					}
					pivot = newVar[0];
					if (choix == imChoice_t.GRAPH) {
						// On fait l'arbre de conflit
						GrapheCNF g = new GrapheCNF(_val.nbVariables);
						g.setValu(_val);
						g.create(_clauses[i]);
						g.toDot(c,pivot);
					}
					addClause(c); // On ajoute la clause au problème
					c.augmenteScore();

					ulong nouvEtage = backtrack(pivot,c); // Et on fait notre backtracking
					if (_mode == mode_t.CL_INTERAC) {
						writeln(c);
						writeln(nouvEtage);
					}
					if (c.isMonome) {
						// Si c est un monome, on peut directement en déduire une valeur
						bool resModif = _val.modif(abs(c.getFirstUnknownLiteral()), c.getFirstUnknownLiteral() > 0, c);
						if(!resModif)
						{
							if(_mode == mode_t.EXPLAIN) {
								try {
									_val.creerContradProof(c, abs(c.getFirstUnknownLiteral()));
								} catch (Exception e) {
								}
							}
							throw new NotSolvable("Monome contradictoire");
						}
						/*debug(2) {
							writeln(needProcess, " est un monomone.");
							readln();
						}*/
					}
				}
			}
		}
	}
	
	public void algo () {
		
		// Premières vérifications
		firstCheck();

		do {
			// Tant qu'on trouve des affectations, on continue
			while (findAffectations()) {
				applyPropagation(); // S'occupe du backtrack si besoin
			}
		} while (fairePari());             // Quand on n'a plus d'affectations trouvées, on fait un nouveau pari si possible, sinon on a fini

		_clauses~=_disabledClauses; // Pour pouvoir tester la solution trouvée en mode debug
		_disabledClauses.length = 0;
	}

	void reset()
	{
		_disabledClauses.length = 0;
		_backtrackLimit.length = 0;
	}

}
